//
//  Momo+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Momo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Momo> {
        return NSFetchRequest<Momo>(entityName: "Momo");
    }

    @NSManaged public var air: String?
    @NSManaged public var dialCode: String?
    @NSManaged public var iso2: String?
    @NSManaged public var max: Int64
    @NSManaged public var min: Int64
    @NSManaged public var name: String?
    @NSManaged public var tele: String?

}
