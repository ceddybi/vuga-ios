//
//  Convos+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Convos {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Convos> {
        return NSFetchRequest<Convos>(entityName: "Convos");
    }

    @NSManaged public var convoid: String?
    @NSManaged public var created_at: String?
    @NSManaged public var last: String?
    @NSManaged public var co: User?
    @NSManaged public var cr: User?

}
