//
//  Rate+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Rate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Rate> {
        return NSFetchRequest<Rate>(entityName: "Rate");
    }

    @NSManaged public var fee_process: Int64
    @NSManaged public var fee_vp: Int64
    @NSManaged public var total: Int64

}
