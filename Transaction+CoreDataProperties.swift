//
//  Transaction+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Transaction {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transaction> {
        return NSFetchRequest<Transaction>(entityName: "Transaction");
    }

    @NSManaged public var add: Int16
    @NSManaged public var all: Int16
    @NSManaged public var transaction_id: String?
    @NSManaged public var typo: String?
    @NSManaged public var typo_msg: String?
    @NSManaged public var typo_title: String?
    @NSManaged public var unix_time: Int64
    @NSManaged public var status: String?
    @NSManaged public var amount: Amount?
    @NSManaged public var from: User?
    @NSManaged public var to: User?

}
