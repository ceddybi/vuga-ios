//
//  Amount+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Amount {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Amount> {
        return NSFetchRequest<Amount>(entityName: "Amount");
    }

    @NSManaged public var amt_int: Int64
    @NSManaged public var amt_str: String?
    @NSManaged public var currency: String?
    @NSManaged public var fee_momo: Int16
    @NSManaged public var fee_process: Int16
    @NSManaged public var fee_vp: Int16
    @NSManaged public var total: Int64
    @NSManaged public var request: Request?
    @NSManaged public var user: User?

}
