#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "RPFloatingPlaceholderConstants.h"
#import "RPFloatingPlaceholderTextField.h"
#import "RPFloatingPlaceholderTextView.h"

FOUNDATION_EXPORT double RPFloatingPlaceholdersVersionNumber;
FOUNDATION_EXPORT const unsigned char RPFloatingPlaceholdersVersionString[];

