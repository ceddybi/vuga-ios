//
//  ChatViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import UIKit
//#import <JSQMessagesViewController/JSQMessages.h>
import JSQMessagesViewController

class ChatViewController: JSQMessagesViewController,
JSQMessagesComposerTextViewPasteDelegate {
    
    var arrayMessages: [JSQMessage] = []
    var userTo: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMessages()
        
        self.automaticallyScrollsToMostRecentMessage = true
        
        self.senderId = CurrentUser.shared.userId()
        self.senderDisplayName = (CurrentUser.shared.user?.username ?? "")
        
        self.title = (userTo.firstname ?? "") + " " + (userTo.lastname ?? "")
        self.collectionView.backgroundColor = UIColor.groupTableViewBackground
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.topItem?.title  = (userTo.firstname ?? "") + " " + (userTo.lastname ?? "")
    }
    
    func loadMessages() {
        
        SocketManager.shared.emitOnToOnConvos(userTo: userTo, handler: { messages in
            
            self.arrayMessages = messages
            self.collectionView.reloadData()
            self.scrollToBottom(animated: true)
        })
    }
    
    @IBAction func profileAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "UserProfileViewController"),
            let userVC = destinationVC as? UserProfileViewController
            else { return }
        userVC.user = userTo
        navigationController?.pushViewController(userVC, animated: true)
    }
    @IBAction func closeAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMessages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: JSQMessagesCollectionViewCell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        //[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
        
        cell.avatarImageView.layer.cornerRadius =  cell.avatarImageView.frame.height/2
        cell.avatarImageView.clipsToBounds = true
        
        let msg = arrayMessages[indexPath.row]
        if !msg.isMediaMessage {
            
            if msg.senderId == userTo.userid {
                
                cell.textView.textColor = UIColor.black
                
                
            } else {
                
                cell.textView.textColor = UIColor.black
                
            }
            
            cell.textView.linkTextAttributes = [ NSForegroundColorAttributeName : cell.textView.textColor];
            
        }
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!{
        
        return arrayMessages[indexPath.row]
        
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!){
        
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!{
        
        let msg = arrayMessages[indexPath.row]
        
        if msg.senderId == CurrentUser.shared.user?.userid {
            
            return JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.init(colorLiteralRed: 247/255, green: 219/255, blue: 178/255, alpha: 1))
            
        } else {
            
            return JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.white)
            
        }
        
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!{
        
        let msg = arrayMessages[indexPath.row]
        
        if msg.senderId == userTo.userid {
            
            let imageView = UIImageView()
            imageView.setUserImage(strUrl: userTo.image_path)
            
            if let image = imageView.image{
                return JSQMessagesAvatarImage.init(placeholder: imageView.image)
            }
            return JSQMessagesAvatarImage.init(placeholder: #imageLiteral(resourceName: "user_empty"))
            
        } else {
            
            let imageView = UIImageView()
            imageView.setUserImage(strUrl: CurrentUser.shared.user?.image_path)
            
            if let image = imageView.image{
                return JSQMessagesAvatarImage.init(placeholder: imageView.image)
            }
            return JSQMessagesAvatarImage.init(placeholder: #imageLiteral(resourceName: "user_empty"))
        }
        
    }
    
    func composerTextView(_ textView: JSQMessagesComposerTextView!, shouldPasteWithSender sender: Any!) -> Bool {
        return true
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        if (SocketManager.shared.socket.status == .connected) {
            
            button.isEnabled = false
            
            SocketManager.shared.emitSendMessage(userTo: userTo, msg: text, handler: {
                self.loadMessages()
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                self.finishSendingMessage(animated: true)
                self.inputToolbar.toggleSendButtonEnabled()
                self.automaticallyScrollsToMostRecentMessage = true
                //self.collectionView.scroll
                self.scrollToBottom(animated: true)
            })
        } else {
            
            self.showOkAlert(title: "Can not connect to the server", message: "")
            
        }
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: arrayMessages[indexPath.row].date)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        
    }
    
}
