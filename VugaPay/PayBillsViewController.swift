//
//  PayBillsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit

fileprivate let sectionInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
fileprivate let itemsPerRow: CGFloat = 2

class PayBillsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var collectioView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func doneAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return 5
    }
    
    let arrayImages = [#imageLiteral(resourceName: "airtime"), #imageLiteral(resourceName: "electr"), #imageLiteral(resourceName: "water"), #imageLiteral(resourceName: "hotel"), #imageLiteral(resourceName: "taxi")]
    let arrayTitles = ["Airtime", "Electricity", "Water", "Hotel", "Taxi"]
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: BillCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BillCollectionViewCell
        
        cell.imageView.image = arrayImages[indexPath.row]
        cell.label.text = arrayTitles[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            
            SocketManager.shared.emitXMIN(handler: { MOMOS, _ in
                
                guard let destinationVC = self.getViewController(name: "BillDetailViewController"),
                let mobileMoneyVC = destinationVC as? BillDetailViewController
                    else { return }
                
                mobileMoneyVC.arrayMOMO = MOMOS
                
                mobileMoneyVC.title = self.arrayTitles[indexPath.row]
                
                self.navigationController?.pushViewController(mobileMoneyVC, animated: true)
            })
            
            
        } else {
            
            self.showOkAlert(title: "Not available", message: "")
        }
        
    }
    
}

extension PayBillsViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
