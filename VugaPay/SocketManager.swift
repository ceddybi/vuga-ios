//
//  SocketManager.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/23/17.
//
//

import Foundation
import SocketIO
import SVProgressHUD
import MagicalRecord
import JSQMessagesViewController
import SRMModalViewController
import CWStatusBarNotification
import SystemConfiguration
import UIKit

let APPVERSION = 1

extension SocketManager {
    
    func hasError(data: [Any]) -> Bool {
        
        SVProgressHUD.dismiss()
        
        guard let response = data[0] as? [String: Any],
            let status = response["status"] as? String,
            status == "200" else {
                
                //TODO: 400
                
                if let response = data[0] as? [String: Any],
                    let status = response["status"] as? String,
                    status == "400",
                    let msg = response["msg"] as? String{
                    
                    SRMModalViewController.sharedInstance().hide()
                    
                    SVProgressHUD.showError(withStatus: msg)
                    
                    return true
                }
                
                SVProgressHUD.showError(withStatus: "Error")
                
                return true
        }
        
        return false
    }
    
    func hashableArray(from data: [Any]) -> [[AnyHashable: Any]]? {
        
        if let response = data[0] as? [String: Any],
            let array = response["data"] as? [[AnyHashable: Any]] {
            
            return array
        }
        
        return nil
    }
    
    func dictArray(from data: [Any]) -> [[String: Any]]? {
        
        if let response = data[0] as? [String: Any],
            let array = response["data"] as? [[String: Any]] {
            
            return array
        }
        
        return nil
    }
    
    func jsonDict(from data: [Any]) -> [String: Any]? {
        
        if let response = data[0] as? [String: Any] {
            
            return response
        }
        
        return nil
    }
    
    func hashableDict(from data: [Any]) -> [AnyHashable: Any]? {
        
        if let response = data[0] as? [String: Any],
            let array = response["data"] as? [AnyHashable: Any] {
            
            return array
        }
        
        return nil
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
        
    }
}

class SocketManager {
    
    static let shared = SocketManager()
    let socket: SocketIOClient
    let notification: CWStatusBarNotification!
    
    var handlerRequests: (([Request]) -> Void)?
    var handlerFriends: (([User], [[String: Any]]) -> Void)?
    var handlerSearchFriends: (([User]) -> Void)?
    var addFriend: (() -> Void)?
    var handlerConvos: (([Convos], [[String: Any]]) -> Void)?
    var handlerCreateRequest: (() -> Void)?
    var handlerAllCreditCards: (([Cards]) -> Void)?
    var handlerOnToOnConvos: (([JSQMessage]) -> Void)?
    var handlerSendMessage: (() -> Void)?
    var handlerUploadProfilePhoto: (() -> Void)?
    var handleUploadPassportPhoto: (() -> Void)?
    var handleCreditCardPhoto: (() -> Void)?
    var handlerChangeUsername: (() -> Void)?
    var handlerChangePassword: (() -> Void)?
    var handlerAllTransaction: (([Transaction]) -> Void)?
    var handlerXMIN: (([Momo], BankAmount) -> Void)?
    var handlerGetMomoRate: ((Rate) -> Void)?
    var handlerGetVPRate: ((Rate) -> Void)?
    var handlerPayCreate: (() -> Void)?
    var handlerAddFunds: (() -> Void)?
    var handlerAddCreditCard: (() -> Void)?
    var handlerFetchUser: (() -> Void)?
    var handlerAcceptRequest: (() -> Void)?
    var handlerRejectRequest: (() -> Void)?
    
    init() {
        
        var pass = ""
        
        if let p = CurrentUser.shared.user?.password {
            
            pass = p
        }
        
        socket = SocketIOClient(socketURL: URL(string: BASEURL)!, config: [.log(true), .forcePolling(true), .nsp(chatPath), .connectParams(["p": pass, "userid": CurrentUser.shared.userId()])])
        notification = CWStatusBarNotification.init()
        notification.notificationLabelTextColor = UIColor.black
        notification.notificationLabelBackgroundColor = UIColor.white
        addHandlers()
    }
    
    func connectToServer() {
        
        if isConnectedToNetwork() == false {
            
            SVProgressHUD.showError(withStatus: "No internet connection")
        }
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.notification.display(withMessage: "Connecting...") {
        }
            
        }
        socket.connect()
    }
    
    func disConnectToServer() {
        socket.disconnect()
    }
    
    func addHandlers() {
        
        socket.on("connect") { (data, ack) in
            
            print(data, ack)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.registerForPushNotifications(application: UIApplication.shared)

                self.notification.dismiss(completion: {
                    self.notification.display(withMessage: "Connected", forDuration: 2.5)
                })
            
            self.emitFetchUser(handler: { 
                print("TADA")
            })
        }
        
        socket.on(userFetchPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerFetchUser,
                !self.hasError(data: data),
                let json = self.jsonDict(from: data){
                
                if let version = json["vx"] as? [String: Any],
                    let v = version["vcode"] as? Int {
                    
                    if v != APPVERSION{
                        
                       
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "show"), object: nil, userInfo: ["link": version["url"]])
                    }
                    
                }
                
                MagicalRecord.save({ (context) in
                    
                    let user = User.mr_import(from: (self.hashableDict(from: data)! as Any), in: context)
                    print(user)
                
                    
                }, completion: { (_, _) in
                    
                    CurrentUser.shared.user = User.mr_import(from: (self.hashableDict(from: data)! as Any))
                    
                    print("TADAUSER = \(CurrentUser.shared.userId())")
                    print(CurrentUser.shared.user)

                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update"), object: nil)
                })
                
                handler()
            }
        }
        
        socket.on(getAllRequestsPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerRequests,
                !self.hasError(data: data),
                let requests = self.hashableArray(from: data){
                
                MagicalRecord.save({ (context) in
                    
                    Request.mr_truncateAll(in: context)
                    Request.mr_import(from: requests, in: context)
                    
                }, completion: { (_, _) in
                    
                    handler(Request.mr_findAll() as! [Request])
                })
            }
        }
        
        socket.on(getAllFriendsPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerFriends,
                !self.hasError(data: data),
                let requests = self.hashableArray(from: data){
                
                handler(User.mr_import(from: requests) as! [User], self.dictArray(from: data)!)
            }
        }
        
        socket.on(searchFriendsPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerSearchFriends,
                !self.hasError(data: data),
                let requests = self.hashableArray(from: data){
                
                handler(User.mr_import(from: requests) as! [User])
            }
        }
        
        socket.on(addFriendPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.addFriend,
                !self.hasError(data: data){
                
                handler()
            }
        }
        
        socket.on(getConvosPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerConvos,
                !self.hasError(data: data),
                let requests = self.hashableArray(from: data){
                
                handler(Convos.mr_import(from: requests) as! [Convos], self.dictArray(from: data)!)
            }
        }
        
        socket.on(createRequestPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerCreateRequest,
                !self.hasError(data: data){
                
                handler()
            }
        }
        
        socket.on(getAllCreditCardsPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerAllCreditCards,
                !self.hasError(data: data),
                let cards = self.hashableArray(from: data){
                
                handler(Cards.mr_import(from: cards) as! [Cards])

            }
        }
        
        socket.on(getOnToOnConvos) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerOnToOnConvos,
                !self.hasError(data: data),
                let messages = self.hashableArray(from: data){
                
                var array: [JSQMessage] = []
                
                for message in messages {
                    
                    let msg = JSQMessage.init(senderId: message["userid"] as? String,
                                              senderDisplayName: message["username"] as? String,
                                              date: Date.init(timeIntervalSince1970: (message["unix_time_int"] as! TimeInterval)/1000),
                                              text: message["msg"] as? String)
                    array.append(msg!)
                    
                }
                
                handler(array)
                
            }
        }
        
        socket.on(sendMessasgePoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerSendMessage,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(uploadProfilePhotoPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerUploadProfilePhoto,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(uploadPassportPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handleUploadPassportPhoto,
                !self.hasError(data: data){
                
                self.emitFetchUser(handler: {
                    
                })
                
                handler()
                
            }
        }
        
        socket.on(uploadPhotoCardPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handleCreditCardPhoto,
                !self.hasError(data: data){
                
                handler()
                
                self.emitFetchUser(handler: { 
                    
                })
                
            }
        }
        
        socket.on(changeUsernamePoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerChangeUsername,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(changePasswordPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerChangePassword,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(getAllTransactionsPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerAllTransaction,
                !self.hasError(data: data),
                let transactions = self.hashableArray(from: data){
                
                MagicalRecord.save({ (context) in
                    
                    Transaction.mr_truncateAll(in: context)
                    Transaction.mr_import(from: transactions, in: context)
                    
                }, completion: { (_, _) in
                    
                    handler(Transaction.mr_findAll() as! [Transaction])
                })
                
            }
        }
        
        socket.on(XMINPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerXMIN,
                !self.hasError(data: data),
                let xmins = self.hashableDict(from: data){
                
                let bank = BankAmount.init(max: (xmins["bank"] as! [String: Any])["max"] as! UInt64, min: (xmins["bank"] as! [String: Any])["min"] as! UInt64)


                handler(Momo.mr_import(from: (xmins["momo"] as! [[AnyHashable: Any]])) as! [Momo], bank)
                
            }
        }
        
        socket.on(getMomoRatesPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerGetMomoRate,
                !self.hasError(data: data){
    
                handler(Rate.mr_import(from: data.first!))
                
            }
        }
        
        socket.on(getVPRatesPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerGetVPRate,
                !self.hasError(data: data){
                
                handler(Rate.mr_import(from: data.first!))
                
            }
        }
        
        socket.on(paycreatePoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerPayCreate,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(addFundsPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerAddFunds,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(addCreditCardPoint) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerAddCreditCard,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(requestAccept) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerAcceptRequest,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
        socket.on(requestReject) { (data, ack) in
            
            print(data)
            
            if let handler = self.handlerRejectRequest,
                !self.hasError(data: data){
                
                handler()
                
            }
        }
        
    }
    
    func emitRequests(handler: @escaping (([Request]) -> Void)) {
        
        handlerRequests = handler
        
        SocketManager.shared.socket.emit(getAllRequestsPoint, CurrentUser.shared.userId())
        
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitFriends(handler: @escaping (([User], [[String: Any]]) -> Void)) {
        
        handlerFriends = handler
        
        SVProgressHUD.show(withStatus: "Loading friends")
        
        SocketManager.shared.socket.emit(getAllFriendsPoint, CurrentUser.shared.userId())
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitSearchFriends(query: String, handler: @escaping (([User]) -> Void)) {
        
        handlerSearchFriends = handler
        
        SVProgressHUD.show(withStatus: "Searching users")
        
        SocketManager.shared.socket.emit(searchFriendsPoint, query)
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitAddFriend(user: User, handler: @escaping (() -> Void)) {
        
        addFriend = handler
        
        SVProgressHUD.show(withStatus: "Adding to friends")
        
        SocketManager.shared.socket.emit(addFriendPoint, ["userid": CurrentUser.shared.userId(), "userto": user.userid!])
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitConvos(handler: @escaping (([Convos], [[String: Any]]) -> Void)) {
        
        handlerConvos = handler
        
        SVProgressHUD.show(withStatus: "Getting conversations")
        
        SocketManager.shared.socket.emit(getConvosPoint, CurrentUser.shared.userId())
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitCreateRequest(user: User, handler: @escaping (() -> Void)) {
        
        handlerCreateRequest = handler
        
        SVProgressHUD.show(withStatus: "Sending request")
        
        SocketManager.shared.socket.emit(createRequestPoint, ["from":CurrentUser.shared.userId(),
                                                              "to": user.userid,
                                                              "amt": 100])
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitAllCreditCards(handler: @escaping (([Cards]) -> Void)) {
        
        handlerAllCreditCards = handler
        
        SVProgressHUD.show(withStatus: "Getting cards")
        
        SocketManager.shared.socket.emit(getAllCreditCardsPoint, CurrentUser.shared.userId())
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitOnToOnConvos(userTo: User, handler: @escaping (([JSQMessage]) -> Void)) {
        
        handlerOnToOnConvos = handler
        
        SVProgressHUD.show(withStatus: "Loading")
        
        SocketManager.shared.socket.emit(getOnToOnConvos, ["userid": CurrentUser.shared.userId(), "userto": userTo.userid])
        SVProgressHUD.dismiss(withDelay: 8)

    }
    
    func emitSendMessage(userTo: User, msg: String,  handler: @escaping (() -> Void)) {
        
        handlerSendMessage = handler
        
        SocketManager.shared.socket.emit(sendMessasgePoint, ["userid": CurrentUser.shared.userId(), "userto": userTo.userid, "msg": msg])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitUploadProfilePhoto(image: UIImage, handler: @escaping (() -> Void)) {
        
        handlerUploadProfilePhoto = handler
        
        SVProgressHUD.show(withStatus: "Loading")
        
        SocketManager.shared.socket.emit(uploadProfilePhotoPoint,["userid": CurrentUser.shared.userId(),
            "file_name": "photo",
            "file_data": image.base64Encode()])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitUploadPassportPhoto(image: UIImage, handler: @escaping (() -> Void)) {
        
        handleUploadPassportPhoto = handler
        
        SVProgressHUD.show(withStatus: "Uploading")
        
        SocketManager.shared.socket.emit(uploadPassportPoint,["userid": CurrentUser.shared.userId(),
                                                                  "file_name": "photo",
                                                                  "file_data": image.base64Encode()])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitUploadCreditCardPhoto(image: UIImage, handler: @escaping (() -> Void)) {
        
        handleCreditCardPhoto = handler
        
        SVProgressHUD.show(withStatus: "Uploading")
        
        SocketManager.shared.socket.emit(uploadPhotoCardPoint,["userid": CurrentUser.shared.userId(),
                                                              "file_name": "photo",
                                                              "file_data": image.base64Encode()])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitChangeUsername(newUsername: String, pasword: String,  handler: @escaping (() -> Void)) {
        
        handlerChangeUsername = handler
        
        SocketManager.shared.socket.emit(changeUsernamePoint, ["new username": newUsername, "password": pasword])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitChangePassword(newPassword: String, oldpasword: String,  handler: @escaping (() -> Void)) {
        
        handlerChangePassword = handler
        
        SocketManager.shared.socket.emit(changePasswordPoint, ["new password": newPassword, "old password": oldpasword])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitAllTransactions(handler: @escaping (([Transaction]) -> Void)) {
        
        handlerAllTransaction = handler
       // SVProgressHUD.show(withStatus: "Getting transactions")
        
        SocketManager.shared.socket.emit(getAllTransactionsPoint, ["userid": CurrentUser.shared.userId()])
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitXMIN(handler: @escaping (([Momo], BankAmount) -> Void)) {
        
        handlerXMIN = handler
        
        SVProgressHUD.show(withStatus: "Loading")
        
        SocketManager.shared.socket.emit(XMINPoint, CurrentUser.shared.userId())
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitAPN(token: String) {
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        
        SocketManager.shared.socket.emit(APNPoint, ["userid": CurrentUser.shared.userId(),
                                                    "iosToken": token,
                                                    "iosDevice": UIDevice.current.identifierForVendor!.uuidString,
                                                    "iosWlan": "Unknow",
                                                    "iosLoc": Locale.current.regionCode?.lowercased(),
                                                    "iosAppversion": version])
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitMomoRates(amt: Int64, country2letters: String, handler: @escaping ((Rate) -> Void)) {
        
        handlerGetMomoRate = handler
        
        SVProgressHUD.show(withStatus: "Getting rates")
        
        SocketManager.shared.socket.emit(getMomoRatesPoint, ["userid": CurrentUser.shared.userId(),
                                                             "amt": amt,
                                                             "country": country2letters])
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitVPRates(amt: Int64, handler: @escaping ((Rate) -> Void)) {
        
        handlerGetVPRate = handler
        
        SVProgressHUD.show(withStatus: "Getting rates")
        
        SocketManager.shared.socket.emit(getVPRatesPoint, ["userid": CurrentUser.shared.userId(),
                                                             "amt": amt])
        SVProgressHUD.dismiss(withDelay: 8)
    }
    
    func emitCreatePay(type: TypePay, handler: @escaping (() -> Void)) {
        
        handlerPayCreate = handler
        
        SVProgressHUD.show(withStatus: "Loading")
        
        SocketManager.shared.socket.emit(paycreatePoint, type.params)
        SVProgressHUD.dismiss(withDelay: 8)
       
    }
    
    func emitFetchUser(handler: @escaping (() -> Void)) {
        
        handlerFetchUser = handler
        
        SocketManager.shared.socket.emit(userFetchPoint, ["userid": CurrentUser.shared.userId(), "vcode": APPVERSION])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitAddFunds(cid: String, amt: String, handler: @escaping (() -> Void)) {
        
        handlerAddFunds = handler
        
        SVProgressHUD.show(withStatus: "Sending")
        
        SocketManager.shared.socket.emit(addFundsPoint, ["userid": CurrentUser.shared.userId(),
                                                          "cid":cid,
                                                          "amt":amt])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitAddCreditCard(card_number: String, cvv: String, type: String, exp_date:String, cardholder_name: String, handler: @escaping (() -> Void)) {
        
        handlerAddCreditCard = handler
        
        SVProgressHUD.show(withStatus: "Loading")
        
        SocketManager.shared.socket.emit(addCreditCardPoint, ["userid": CurrentUser.shared.userId(),
                                                           "card": ["card_number": card_number],
                                                           "cvv": cvv,
                                                           "type": type,
                                                           "cardholder_name": cardholder_name,
                                                           "exp_date": exp_date])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitAcceptRequest(requestID: String, handler: @escaping (() -> Void)) {
        
        handlerAcceptRequest = handler
        
        SocketManager.shared.socket.emit(requestAccept, ["userid": CurrentUser.shared.userId(), "rid": requestID])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
    func emitRejectRequest(requestID: String, handler: @escaping (() -> Void)) {
        
        handlerRejectRequest = handler
        
        SocketManager.shared.socket.emit(requestReject, ["userid": CurrentUser.shared.userId(), "rid": requestID])
        SVProgressHUD.dismiss(withDelay: 8)
        
    }
    
}

enum TypePay {
    case mobile(params: MobilePay)
    case vuga(params: VugaPay)
    case airtime(params: Airtime)
    
    var params: [String: Any]{
      
        switch self {
        case .mobile(let params):
            return ["typo": "momo",
                "userid": CurrentUser.shared.userId(),
            "country":params.country,
            "phone":params.phone,
            "amount": ["amt_str": "\(params.amt_int)",
                "amt_int": params.amt_int]]
        case .vuga(let params):
            return ["typo":"vp",
                    "sR": false,
            "userid": CurrentUser.shared.userId(),
            "amount":["total": params.total],
            "to": ["userid":params.user.userid,
                "firstname": params.user.firstname,
                "lastname": params.user.lastname,
                "username": params.user.username]]
            
        case .airtime(let params):
            
            
            return ["typo": "bill",
                    "userid": CurrentUser.shared.userId(),
            "bid": "at",
            "country": "rw",
            "phone": params.phone,
            "amt": "amt"]
        }
    }
    
    
}


struct MobilePay {
    var country: String
    var phone: String
    var amt_str: String
    var amt_int: UInt64
}

struct VugaPay {
    
    var user: User
    var total: String
    
}

struct Airtime {
    
    var phone: String
    var amt: String
}








