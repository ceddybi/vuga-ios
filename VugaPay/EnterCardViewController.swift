//
//  EnterCardViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/16/17.
//
//

import UIKit
import MFCard

class EnterCardViewController: UIViewController, MFCardDelegate {
    @IBOutlet weak var creditCard: MFCardView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        creditCard.autoDismiss = true
        creditCard.toast = false
        creditCard.delegate = self
        creditCard.controlButtonsRadius = 5
        creditCard.cardRadius = 5

    }
    
    func cardDoneButtonClicked(_ card: Card?, error: String?) {
        if let card = card,
            error == nil{
   
            let cardNumber = card.number!
            self.view.makeToast("\(cardNumber) Card added")
            
            SocketManager.shared.emitAddCreditCard(card_number: card.number!,
                                                   cvv: card.cvc!,
                                                   type: (card.cardType?.rawValue)!,
                                                   exp_date: card.month! + String(card.year!.characters.suffix(2)),
                                                   cardholder_name: card.name!,
                                                   handler: {
                                                    
                                                    
            })
            
        }else{
            print(error!)
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func cardTypeDidIdentify(_ cardType: String) {
        print(cardType)
        
        // Notes -
        
        // You can change Card background and other parameters once Card Type is identified
        // e.g - myCard?.cardImage  will help set Card Image
    }

}
