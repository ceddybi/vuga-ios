//
//  MessagesViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var convoses: [Convos] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(name: "UserTableViewCell")
        
        convoses = CurrentUser.shared.getConvos()

        SocketManager.shared.emitConvos { convos, array in
            
            CurrentUser.shared.saveConvos(friends: array)
            self.convoses = CurrentUser.shared.getConvos()
            self.tableView.reloadData()
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count > 0 {
            
            convoses = CurrentUser.shared.getConvos().filter({ (convos) -> Bool in
                
                if var user = convos.co {
                    
                    if user.userid == CurrentUser.shared.userId(),
                        let userCR = convos.cr{
                        
                        user = userCR
                    }
                    
                    let fullName = (user.firstname ?? "") +  (user.lastname ?? "")
                    
                    return fullName.lowercased().contains(searchText.lowercased())
                }
                return true
            })
            
        } else {
            convoses = CurrentUser.shared.getConvos()
            
        }
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return convoses.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.userCell(at: indexPath)
        
        if var user = convoses[indexPath.row].co {
            
            if user.userid == CurrentUser.shared.userId(),
                let userCR = convoses[indexPath.row].cr{
                
                user = userCR
            }
            
            if user.userid == CurrentUser.shared.userId(),
                (convoses[indexPath.row].co?.count_int)! > 0{
                
                cell.contentView.backgroundColor = UIColor.groupTableViewBackground
            } else if (convoses[indexPath.row].cr?.count_int)! > 0{
                
                cell.contentView.backgroundColor = UIColor.groupTableViewBackground
            }
  
            cell.configureCell(user: user, type: .conversetion(lastList: convoses[indexPath.row].last))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let destinationVC = getViewController(name: "ChatViewController"),
        let chatVC = destinationVC as? ChatViewController,
        let navVC = getViewController(name: "ChatNavigationController") as? UINavigationController
            else { return }
        
        if var user = convoses[indexPath.row].cr {
            
            if user.userid == CurrentUser.shared.userId(),
                let userCR = convoses[indexPath.row].co{
                
                user = userCR
            }
            
            chatVC.userTo = user
        }

        
        navVC.setViewControllers([chatVC], animated: true)
        
        self.present(navVC, animated: true)

    }
    
    @IBAction func doneAction(_ sender: Any) {
         navigationController?.dismiss(animated: true)
    }

}
