//
//  ProfilePhotoViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import UIKit
import ALCameraViewController

class ProfilePhotoViewController: UIViewController {
    
    var params: RegisterParams!

    @IBOutlet weak var profileImageView: UIImageView!
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SocketManager.shared.connectToServer()
    }

    @IBAction func continueAction(_ sender: Any) {
        
        if let image = image {
                                        
                                        SocketManager.shared.emitUploadProfilePhoto(image: image, handler: {
                                            
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            
                                            appDelegate.mainFlow()
                                        
            })
            
            
        } else {
            
            self.showOkAlert(title: "Choose profile photo", message: "")
        }
        
    }
    
    @IBAction func tapOnImage(_ sender: Any) {
        
        let croppingEnabled = true
        let cameraViewController = CameraViewController(croppingEnabled: croppingEnabled) { [weak self] image, asset in
            
           self?.image = image
            
            self?.profileImageView.image = image
            
            self?.dismiss(animated: true, completion: nil)
        }
        
             self.present(cameraViewController, animated: true)
        
    }
}
