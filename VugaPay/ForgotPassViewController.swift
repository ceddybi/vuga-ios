//
//  ForgotPassViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/9/17.
//
//

import UIKit

class ForgotPassViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func resetPasswordAction(_ sender: Any) {
        
        AuthManager.resetPassword(email: emailTextField.text!) { 
            
            
        }
    }
    
    @IBAction func tapOnView(_ sender: Any) {
        
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "RegistrationViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }
}
