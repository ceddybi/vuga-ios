//
//  TransactionTableViewCell.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 2/1/17.
//
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with transaction: Transaction) {
        
        var dollars: Double = 0
        
        if let amount = transaction.amount?.amt_str,
            let amountDouble = Double(amount) {
            
            dollars = amountDouble/100
        }
        
        amountLabel.text = "$" + "\(dollars)"
        let dateFromatter = DateFormatter()
        dateFromatter.dateStyle = .medium
        let date = dateFromatter.string(from: Date.init(timeIntervalSince1970:(Double(transaction.unix_time) )/1000))
        dateLabel.text = date
        
        nameLabel.text = transaction.typo_title
        
        if let status = transaction.status,
        status == "pend"{
            
           imageViewUser.image = #imageLiteral(resourceName: "pend")
        } else {
            
            imageViewUser.image = #imageLiteral(resourceName: "tick")
        }
        
//        var user: User?
//        
//        if transaction.from?.userid == CurrentUser.shared.userId() {
//            
//            user = transaction.to
//            
//        } else {
//
//            user = transaction.from
//        }
//        
//        if let currentUser = user {
//            
//                 imageViewUser.setUserImage(strUrl: user?.image_path)
//            
//            if currentUser.userid != nil{
//            
//                nameLabel.text = (currentUser.firstname ?? "") + " " + (currentUser.lastname ?? "")
//            
//            } else {
//            
//                nameLabel.text = transaction.to?.phone
//            
//            }
//            
//        }
    }
    
}
