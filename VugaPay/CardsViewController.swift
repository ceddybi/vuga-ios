//
//  CardsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/16/17.
//
//

import UIKit
import SRMModalViewController
import ALCameraViewController

class CardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var arrayCards: [Cards] = []

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        SocketManager.shared.emitAllCreditCards { cards in
            
            self.arrayCards = (cards as! [Cards])
            self.tableView.reloadData()
            
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayCards.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cell")
        
        let card = arrayCards[indexPath.row]
        
        cell.textLabel?.text = card.card_number
        
        switch card.status! {
        case "":
            cell.detailTextLabel?.text = "Verify now"
            cell.detailTextLabel?.textColor = UIColor.black
            break
        case "pend":
            cell.detailTextLabel?.text = "Pending"
            cell.detailTextLabel?.textColor = UIColor.orange
            break
        case "fail":
            cell.detailTextLabel?.text = "Failed to verify"
            cell.detailTextLabel?.textColor = UIColor.red
            break
        case "done":
            cell.detailTextLabel?.text = "Verified"
            cell.detailTextLabel?.textColor = UIColor.green
            break
        default:
            break
        }
        
        switch card.type! {
        case "visa":
            cell.imageView?.image = #imageLiteral(resourceName: "visa")
            break
        case "mastercard":
            cell.imageView?.image = #imageLiteral(resourceName: "mastercard")
            break
        case "amex":
            cell.imageView?.image = #imageLiteral(resourceName: "amex")
            break
        case "discover":
            cell.imageView?.image = #imageLiteral(resourceName: "discover")
            break
        default:
            cell.imageView?.image = #imageLiteral(resourceName: "unknown_cc")
            break
        }
        
        cell.imageView?.frame.size = CGSize.init(width: 40, height: 40)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.checkStatusCard(card: arrayCards[indexPath.row])
    }

    @IBAction func addCardAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "EnterCardViewController")
            else { return }
        
        navigationController?.show(destinationVC, sender: self)
    }
    
    func checkStatusCard(card: Cards) {
        
        if card.status == "" {
            
            showConfrimAlert(title: "Please send card's photo", message: "", handler: {
                
                self.uploadPhoto()
            })
            
        }
        if card.status == "pend" {
            
            self.showOkAlert(title: "Card in pending", message: "")
            
        }
        if card.status == "fail" {
            
            showConfrimAlert(title: "Warning", message: "Please send again card's photo", handler: { 
                
               self.uploadPhoto()
            })
            
        }
        if card.status == "done" {
            
            showConfrimAlert(title: "Do you want add tunds from this card?", message: "", handler: {
                
                guard let destinationVC = self.getViewController(name: "SendFundsViewController"),
                let addFundsVC = destinationVC as? SendFundsViewController
                    else { return }
                
                addFundsVC.card = card
                
                self.navigationController?.pushViewController(destinationVC, animated: true)
            })
            
        }
        
    }
    
    func uploadPhoto() {
        
        guard let destinationVC =  self.getViewController(name: "PassportVerifyViewController"),
            let passportVC = destinationVC as? PassportVerifyViewController
            else { return }
        
        passportVC.creditCard = true
        
        passportVC.handler = {
            
            let croppingEnabled = true
            let cameraViewController = CameraViewController(croppingEnabled: croppingEnabled) { [weak self] image, asset in
                
                guard let destinationVC =  self?.getViewController(name: "PassportVerifyViewController"),
                    let passportVC = destinationVC as? PassportVerifyViewController
                    else { return }
                
                passportVC.image = image
                
                destinationVC.view.frame = CGRect.init(x: 0,
                                                       y: 0,
                                                       width: 300,
                                                       height: 330)
                destinationVC.view.layer.cornerRadius = 10
                SRMModalViewController.sharedInstance().showView(with: passportVC)
                
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true)
            
        }
        
        destinationVC.view.frame = CGRect.init(x: 0,
                                               y: 0,
                                               width: 300,
                                               height: 330)
        destinationVC.view.layer.cornerRadius = 10
        SRMModalViewController.sharedInstance().showView(with: passportVC)
    }
}
