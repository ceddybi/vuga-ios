//
//  ConfirmTransactionViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/16/17.
//
//

import UIKit
import SRMModalViewController

enum TypeConfirm {
    case sendRequest
    case sendPhoneMoney
    case sendVugaPay
}

class ConfirmTransactionViewController: UIViewController {
    
    var user: User!
    var typeTransaction = TypeConfirm.sendRequest
    var countryCode: String!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var processingfeeLabel: UILabel!
    @IBOutlet weak var vugaPayFeeLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func configurePhoneTransaction(rate: Rate, phone: String, countryCode: String, flag: UIImage) {
        
        nameLabel.text = phone
        usernameLabel.text = phone
        processingfeeLabel.text = "\(rate.fee_process)"
        vugaPayFeeLabel.text = "\(rate.fee_vp)"
        totalLabel.text = "\(Double(rate.total)/100)"
        self.countryCode = countryCode
        imageView.image = flag
    }
    
    func configureVugaPayTransaction(rate: Rate, user: User) {
        
        nameLabel.text = (user.firstname ?? "") + " " + (user.lastname ?? "")
        usernameLabel.text = (user.username ?? "")
        processingfeeLabel.text = "\(rate.fee_process)"
        vugaPayFeeLabel.text = "\(rate.fee_vp)"
        totalLabel.text = "$" + "\(Double(rate.total)/100)"
        self.imageView.setUserImage(strUrl: user.image_path)
        self.user = user

    }
    
    func configureVugaPayRequest(rate: String, user: User) {
        self.imageView.setUserImage(strUrl: user.image_path)
        nameLabel.text = (user.firstname ?? "") + " " + (user.lastname ?? "")
        usernameLabel.text = (user.username ?? "")
        totalLabel.text = rate
        self.user = user
        
    }

    @IBAction func payNowAction(_ sender: Any) {
        
        guard let destinationVC =  getViewController(name: "VerifyUserViewController"),
        let verifVC = destinationVC as? VerifyUserViewController
            else { return }
        
        verifVC.handler = {
            
             SRMModalViewController.sharedInstance().hide()
            
            switch self.typeTransaction {
            case .sendRequest:
                
                SocketManager.shared.emitCreateRequest(user: self.user, handler: {
                    
                    self.navigationController?.dismiss(animated: true)
                    
                })
                
                break
            case .sendPhoneMoney:
                
                let amount = self.totalLabel.text?.replacingOccurrences(of: ".", with: "")
                
                let params =  MobilePay.init(country: self.countryCode,
                                             phone: "+" + self.nameLabel.text!,
                                             amt_str: self.totalLabel.text!,
                                             amt_int: UInt64(amount!)!)
                SocketManager.shared.emitCreatePay(type: .mobile(params: params), handler: { 
                   
                    self.navigationController?.dismiss(animated: true)
                    
                })
                
                break
                
            case .sendVugaPay:
                
                var amount = self.totalLabel.text!.replacingOccurrences(of: ".", with: "")
                
                amount = amount.replacingOccurrences(of: "$", with: "")
                
                  let params = VugaPay.init(user: self.user, total: amount)
                
                SocketManager.shared.emitCreatePay(type: .vuga(params: params), handler: {
                    
                    self.navigationController?.dismiss(animated: true)
                    
                })
                
                break
            }
            
        }
        
        destinationVC.view.frame = CGRect.init(x: 0,
                                               y: 0,
                                               width: 250,
                                               height: 250)
        destinationVC.view.layer.cornerRadius = 10
        SRMModalViewController.sharedInstance().showView(with: verifVC)
  
    }
}
