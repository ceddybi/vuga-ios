//
//  PaymentRequestsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit
import SocketIO
import SVProgressHUD

class PaymentRequestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var requests: [Request] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.requests = Request.mr_findAll() as! [Request]
        
        update()
    }
    
    func update() {
        
        SVProgressHUD.show(withStatus: "Loading requests")
        
        SocketManager.shared.emitRequests { requests in
            
            self.requests = requests
            
            self.tableView.reloadData()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return requests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cell")
        
       // cell.textLabel?.text = "RE: " + (requests[indexPath.row].from?.firstname)! + " " + (requests[indexPath.row].from?.lastname)!
        
        var dollars: Double = 0
        
        if let amount = requests[indexPath.row].amount?.amt_str,
            let amountDouble = Double(amount) {
            
            dollars = amountDouble/100
        }
        
        cell.detailTextLabel?.text = (requests[indexPath.row].status ?? "") + "   $" + "\(dollars)"
        
        if requests[indexPath.row].status == "pend" {
            
            cell.imageView?.image = #imageLiteral(resourceName: "pend")
        } else if requests[indexPath.row].status == "reject" {
            
            cell.imageView?.image = #imageLiteral(resourceName: "rej")
         
        } else {
            
            //TODO:
            cell.imageView?.image = #imageLiteral(resourceName: "tick")
        }
           cell.imageView?.contentMode = .scaleAspectFit
        
        if let userFrom = requests[indexPath.row].from,
            userFrom.userid != CurrentUser.shared.userId(){
            
            cell.textLabel?.text = "RE: " + (userFrom.firstname)! + " " + (userFrom.lastname)!
            
        } else if let userTo = requests[indexPath.row].to {
            
           cell.textLabel?.text = "RE: " + (userTo.firstname ?? "") + " " + (userTo.lastname ?? "")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if requests[indexPath.row].status == "pend" {
        
        let request = self.requests[indexPath.row]
        
        if let userFrom = request.from,
            userFrom.userid != CurrentUser.shared.userId() {
            
            let name = (userFrom.firstname ?? "") + " " +  (userFrom.lastname ?? "")
            
            var dollars: Double = 0
            
            if let amount = requests[indexPath.row].amount?.amt_str,
                let amountDouble = Double(amount) {
                
                dollars = amountDouble/100
            }
            
            showConfrimRejectAlert(title: "\(name)", message: "Do you want confirm request from \(name) with amount $\(dollars)!", user: userFrom, handler: {
                
                SocketManager.shared.emitAcceptRequest(requestID: request.request_id!, handler: {
                    self.update()
                })
  
            }, noHandler: { 
  
                SocketManager.shared.emitRejectRequest(requestID: request.request_id!, handler: {
                    self.update()
                })
            })
            }
        }
    }

    @IBAction func doneAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
    
    @IBAction func findFriendAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "SelectSearchFriendViewController"),
        let searchVC = destinationVC as? SelectSearchFriendViewController
            else { return }
        
        searchVC.typeSearch = .sendRequest
        
        navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    @IBAction func sendRequest(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "SelectSearchFriendViewController"),
            let searchVC = destinationVC as? SelectSearchFriendViewController
            else { return }
        
        searchVC.typeSearch = .sendRequest
        
        navigationController?.pushViewController(destinationVC, animated: true)
    }
}
