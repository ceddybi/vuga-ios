//
//  UIButton+Round.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/11/17.
//
//

import Foundation
import UIKit

extension UIButton {
    
    func roundWhiteButton() {
        
        self.layer.cornerRadius = 20
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.white.cgColor
        
    }
    
}
