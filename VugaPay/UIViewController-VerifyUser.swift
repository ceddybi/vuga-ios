//
//  UIViewController-VerifyUser.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/25/17.
//
//

import Foundation
import UIKit
import SRMModalViewController
import ALCameraViewController

extension UIViewController {
    
    func allowTransaction() -> Bool {
            
            switch CurrentUser.shared.userStatus() {
                
            case .done:
                
                return true
                
            case .uploadPassport:
                
                self.showConfrimAlert(title: "Verify account", message: "You have verify your account in order to continue using VugaPay", handler: {
                    
                    self.openUploadPasswortViewController()
                    
                })
                
                return false
                
            case .uploadPassportAgain:
                
                self.showConfrimAlert(title: "Verify account again", message: "You have verify your account in order to continue using VugaPay", handler: { 
                    
                    self.openUploadPasswortViewController()
                    
                })
                
                self.showOkAlert(title: "Account pending", message: "Your account is pending verification, please wait our team is verifying your account, we will get back you shortly")
                
                return false
                
            case .pending:
                
                self.showOkAlert(title: "Account pending", message: "Your account is pending verification, please wait our team is verifying your account, we will get back you shortly")
                
                return false
            }
            
    }
    
    func openUploadPasswortViewController() {
        
        guard let destinationVC =  self.getViewController(name: "PassportVerifyViewController"),
        let passportVC = destinationVC as? PassportVerifyViewController
            else { return }
        
        passportVC.handler = {
            
            let croppingEnabled = true
            let cameraViewController = CameraViewController(croppingEnabled: croppingEnabled) { [weak self] image, asset in
                
                guard let destinationVC =  self?.getViewController(name: "PassportVerifyViewController"),
                    let passportVC = destinationVC as? PassportVerifyViewController
                    else { return }
                
                passportVC.image = image
                
                destinationVC.view.frame = CGRect.init(x: 0,
                                                       y: 0,
                                                       width: 300,
                                                       height: 330)
                destinationVC.view.layer.cornerRadius = 10
                SRMModalViewController.sharedInstance().showView(with: passportVC)
                
                self?.dismiss(animated: true, completion: nil)
            }
            
            self.present(cameraViewController, animated: true)
            
        }
        
        destinationVC.view.frame = CGRect.init(x: 0,
                                               y: 0,
                                               width: 300,
                                               height: 330)
        destinationVC.view.layer.cornerRadius = 10
        SRMModalViewController.sharedInstance().showView(with: passportVC)
        
}

}
