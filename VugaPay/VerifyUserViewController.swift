//
//  VerifyUserViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/23/17.
//
//

import UIKit

class VerifyUserViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var invalidLabel: UILabel!
    
    var handler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }

    @IBAction func confirmAction(_ sender: Any) {
        
        if CurrentUser.shared.getPassword() == passwordTextField.text{
            
            invalidLabel.isHidden = true
            
            if let handler = handler {
                
                handler()
            }
            
        } else {
            
            invalidLabel.isHidden = false
            
        }
        
        
    }
    
    @IBAction func tapOnView(_ sender: Any) {
        
        self.view.endEditing(true)
    }
}
