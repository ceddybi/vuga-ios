//
//  ChangePasswordViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/25/17.
//
//

import UIKit
import SRMModalViewController
import SVProgressHUD

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var repeatNewPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }

    @IBAction func continueAction(_ sender: Any) {
        
        if let pass = CurrentUser.shared.user?.password,
            pass == oldPassword.text! {
            
            if newPassword.text! == repeatNewPassword.text! {
                
                SocketManager.shared.emitChangePassword(newPassword: newPassword.text!, oldpasword: oldPassword.text!, handler: {
                    
                    CurrentUser.shared.setPassword(password: self.newPassword.text!)
                   
                    SRMModalViewController.sharedInstance().hide()
                    
                })
                
                
            } else {
                
                SRMModalViewController.sharedInstance().hide()
                
                SVProgressHUD.showError(withStatus: "Passwords do not equals")
                
            }
        
        } else {
            
            SRMModalViewController.sharedInstance().hide()
            
            SVProgressHUD.showError(withStatus: "Wrong password")
        }
        
    }
}
