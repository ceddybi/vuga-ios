//
//  CountryUIViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 2/4/17.
//
//

import UIKit

class CountryUIViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var filerArray: [String]?
    var arrayCountries: [[String: Any]] = []
    var headerKeys: [String] = []
    var handlerCountry: (([String: Any], UIImage) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createDataSource()
        
    }
    
    func createDataSource() {
        
        let string = try! String.init(contentsOfFile: self.resourceBundle().path(forResource: "phone_country_code", ofType: "json")!)
        
        let array = try! JSONSerialization.jsonObject(with: string.data(using: .utf8)!, options: []) as! [[String : Any]]
        
        if let filters = filerArray {
            
            var arrayDict: [[String: Any]] = []
            
            for countryCode in filters {
                
                for country in array {
                    
                    if countryCode == (country["country_code"] as? String)! {
                        
                       arrayDict.append(country)
                        
                    }
                    
                }
                
            }
            
            arrayCountries = arrayDict
            
        } else {
            
            arrayCountries = array
        }
        
    }
    
    func resourceBundle() -> Bundle {
        
        return Bundle(path: Bundle.main.path(forResource: "Phone-Country-Code-and-Flags", ofType: "bundle")!)!
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int{
        
       return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
       return arrayCountries.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        
        let dict = arrayCountries[indexPath.row]
        
        cell.textLabel?.text = (dict["country_en"] as? String)! + " " + (dict["country_code"] as? String)!
        
        cell.imageView?.image = imageForCountryCode(code: (dict["country_code"] as? String)!)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let handlerCountry = handlerCountry {
            
            handlerCountry(arrayCountries[indexPath.row], imageForCountryCode(code: (arrayCountries[indexPath.row]["country_code"] as? String)!))
        }
        
        self.dismiss(animated: true)
    }
    
    func imageForCountryCode(code: String) -> UIImage {
        
        let stringJSON = try! String.init(contentsOfFile: resourceBundle().path(forResource: "flag_indices", ofType: "json")!)
        
        let dict = try! JSONSerialization.jsonObject(with: stringJSON.data(using: .utf8)!, options: []) as! [String: Int]
        let cgImage = UIImage.init(contentsOfFile: resourceBundle().path(forResource: "flags", ofType: "png")!)?.cgImage

        let image = cgImage!.cropping(to: CGRect.init(x: 0, y: dict[code]!*2, width: 32, height: 32))

        let result = UIImage.init(cgImage: image!, scale: 2, orientation: .up)
        return result
    }

}
