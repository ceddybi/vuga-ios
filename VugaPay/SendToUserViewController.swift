//
//  SendToUserViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/16/17.
//
//

import UIKit

class SendToUserViewController: UIViewController, UITextFieldDelegate {
    
    var user: User!
    var bankAmount: BankAmount!
    var isModally = false

    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isModally {
            
            let item = UIBarButtonItem.init(title: "Close", style: .done, target: self, action: #selector(SendToUserViewController.close))
            self.navigationItem.leftBarButtonItem = item
            
        }

        nameLabel.text = (user.firstname ?? "") + (user.lastname ?? "")
        userNameLabel.text = "@" + (user.username ?? "")
        imageView.setUserImage(strUrl: user.image_path)
        
    }
    
    func close() {
        
        self.navigationController?.dismiss(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.setNumberCurrencies(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
    }
    
    
    @IBAction func continueAction(_ sender: Any) {
        
        if (amountTextField.text?.characters.count)! == 0 {
            
            return
        }
        
        if let _ = bankAmount {
            
            let str = amountTextField.text?.replacingOccurrences(of: "$", with: "")
            
            let doubleStr = Double(str!)!*100
            let amountInt = Int64(doubleStr)
            
            amountTextField.text = amountTextField.text?.replacingOccurrences(of: "$", with: "")
            
            SocketManager.shared.emitVPRates(amt: amountInt) { (rate) in
                
                guard let destinationVC = self.getViewController(name: "ConfirmTransactionViewController"),
                    let confirmVC = destinationVC as? ConfirmTransactionViewController
                    else { return }
                
                confirmVC.typeTransaction = .sendVugaPay
                
                self.navigationController?.pushViewController(confirmVC, animated: true)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    confirmVC.configureVugaPayTransaction(rate: rate, user: self.user)
                }
            }
            
        } else {
            
            guard let destinationVC = self.getViewController(name: "ConfirmTransactionViewController"),
                let confirmVC = destinationVC as? ConfirmTransactionViewController
                else { return }
            
            confirmVC.typeTransaction = .sendRequest
            
            self.navigationController?.pushViewController(confirmVC, animated: true)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                confirmVC.configureVugaPayRequest(rate: self.amountTextField.text!, user: self.user)
            }
        }
    }
}
