//
//  InRecordsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit
import SVProgressHUD

class InRecordsViewController: UIViewController, UITableViewDataSource {
    
    var transactionIn: [Transaction] = []

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(name: "TransactionTableViewCell")
        
        self.filterTransaction(transactions: Transaction.mr_findAll() as! [Transaction])
        
        SocketManager.shared.emitAllTransactions { transactions in
            
           self.filterTransaction(transactions: transactions)
        }
    }
    
    func filterTransaction(transactions: [Transaction]) {
        
        var array: [Transaction] = []
        
        for transaction in transactions {
            
            if transaction.to?.userid == CurrentUser.shared.userId() {
                
                array.append(transaction)
            }
            
        }
        self.transactionIn = array
        self.tableView.reloadData()
    }

    @IBAction func doneAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return transactionIn.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.transactionCell(at: indexPath)
        
        cell.configure(with: transactionIn[indexPath.row])
        
        return cell
        
    }
}
