//
//  PhoneFormatter.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/27/17.
//
//

import UIKit
import SinchVerification
import libPhoneNumber_iOS

class PhoneFormatter: NSObject {

    static func setTextFieldPlaceHolder(_ textFiled: UITextField, region: String) {
        
        let textF = textFiled
      
        let formatter = SINUITextFieldPhoneNumberFormatter.init(countryCode: region)
        formatter.textField = textF
        
        textFiled.setValue(UIColor.darkGray, forKeyPath: "_placeholderLabel.textColor")
        
        textFiled.placeholder = formatter.exampleNumber(with: .national)
        
        textFiled.text = ""
    }
    
    static func validateTextfield(textField: UITextField, region: String, range: NSRange, withString: String) {
        
        let phoneUtil = NBPhoneNumberUtil()
        let myNumber = try? phoneUtil.parse(NSString(string: textField.text!).replacingCharacters(in: range, with: withString),
                                            defaultRegion: region)

//        let valid = try? SINPhoneNumberUtil().isPossibleNumber(NSString(string: textField.text!).replacingCharacters(in: range, with: withString), fromRegion: region)
        
        if phoneUtil.isValidNumber(myNumber){
            textField.textColor = UIColor.black
        } else {
            
            textField.textColor = UIColor.red
        }
    }
    
    static func validatePhone(phone: String, region: String) -> Bool {
        
        let phoneUtil = NBPhoneNumberUtil()
        let myNumber = try? phoneUtil.parse(phone,
                                            defaultRegion: region)
        
        return phoneUtil.isValidNumber(myNumber)
    }
    
}
