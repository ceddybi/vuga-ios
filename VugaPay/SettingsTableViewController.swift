//
//  SettingsTableViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/11/17.
//
//

import UIKit
import SRMModalViewController
import ALCameraViewController
import SVProgressHUD

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUserInfo()
    }
    
    func setUserInfo() {
        
        if let user = CurrentUser.shared.user {
            
            profileImageView.setUserImage(strUrl: user.image_path)
            usernameLabel.text = (user.firstname ?? "") + (user.lastname ?? "")
            phoneLabel.text = user.phone
            amountLabel.text = CurrentUser.shared.amount()
            
        }
        
    }

    @IBAction func tapOnImage(_ sender: Any) {
        
        let croppingEnabled = true
        let cameraViewController = CameraViewController(croppingEnabled: croppingEnabled) { [weak self] image, asset in
            
            self?.image = image
            
            self?.profileImageView.image = image
            
            self?.updatePhoto()
            
            self?.dismiss(animated: true, completion: nil)
        }
        
        self.present(cameraViewController, animated: true)
        
    }
    
    func updatePhoto() {
        
        if let image = image {
            
            SocketManager.shared.emitUploadProfilePhoto(image: image, handler: {
                
                SVProgressHUD.showSuccess(withStatus: "Photo updated")

            })
            
            
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section  {
        case 2:
            
            if indexPath.row == 0 {
                
                guard let destinationVC = getViewController(name: "SearchFriendsNavigationController")
                    else { return }
                
                self.present(destinationVC, animated: true)
                
            }
            
            if indexPath.row == 1 {
                
                guard let destinationVC =  getViewController(name: "ChangeUsernameViewController")
                    else { return }
                
                destinationVC.view.frame = CGRect.init(x: 0,
                                                       y: 0,
                                                       width: 300,
                                                       height: 330)
                destinationVC.view.layer.cornerRadius = 10
                SRMModalViewController.sharedInstance().showView(with: destinationVC)
                
            }
            
            break
            
        case 3:
           
           if indexPath.row == 0 {
            
            guard let destinationVC = getViewController(name: "ChangePasswordViewController")
                else { return }
            
            destinationVC.view.frame = CGRect.init(x: 0,
                                                   y: 0,
                                                   width: 300,
                                                   height: 330)
            destinationVC.view.layer.cornerRadius = 10
            SRMModalViewController.sharedInstance().showView(with: destinationVC)
            
           }
            if indexPath.row == 1 {
                
                guard let destinationVC = getViewController(name: "HelpViewController")
                    else { return }
                
                self.present(destinationVC, animated: true)
                
            }
            
            break
            
        default:
            break
        }
        
    }

}
