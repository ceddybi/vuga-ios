//
//  EnterCodeViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import UIKit

struct RegisterParams {
    
    var phone: String?
    var firstname: String?
    var lasttname: String?
    var username: String?
    var email: String?
    var password: String?
}

class EnterCodeViewController: UIViewController, UITextFieldDelegate {
    
    var code: String!
    var params: RegisterParams!

    @IBOutlet weak var codeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeTextField.delegate = self
      
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if code == newString {
            
            guard let destinationVC = getViewController(name: "InfoRegistartionViewController"),
            let infoVC = destinationVC as? InfoRegistartionViewController
            else { return true}
            
            infoVC.params = self.params
            textField.textColor = UIColor.black
            
            navigationController?.pushViewController(infoVC, animated: true)
            
        } else {
            
            textField.textColor = UIColor.red
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }

    @IBAction func sendAction(_ sender: Any) {
        
        let value = UserDefaults.standard.value(forKey: "time") as! Double
        
        if (Date().timeIntervalSince1970 - value) > 20 {
     
            AuthManager.generateSMSCode(phone: self.params.phone!, code: UserDefaults.standard.value(forKey: "code") as! UInt64) { (code) in
                
                print(code)
                
                self.code = "\(code)"
                
            }
            
        } else {
            
            self.showOkAlert(title: "Wait for two minutes", message: "")
            
        }
        
       self.view.endEditing(true)
    }
    
    @IBAction func tapOnView(_ sender: Any) {
        
        self.view.endEditing(true)
    }
}
