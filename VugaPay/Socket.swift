//
//  Socket.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/21/17.
//
//

import Foundation
import SocketIO

class Socket {
    
    static let shared = Socket()
    
    let socketChat: SocketIOClient
    
    init() {
        
        self.socketChat = SocketIOClient.init(socketURL:  URL.init(string: "https://vn.vugapay.com/chat")!, config: [.log(true)])
        
        self.socketChat.connect()
        
        self.socketChat.onAny { (event) in
            print(event)
        }
        
        self.socketChat.on("connect") {data, ack in
            print(data, ack)
            
            self.socketChat.emitWithAck("req.get.all", "", "Sdhy4cBpBUn7JWcVpV0csOyzvkNn84").timingOut(after: 2, callback: { (data) in
                
                print(data)
                
            })
        }
        
    }
    
    func establishConnection() {
        socketChat.connect()
    }
    
}
