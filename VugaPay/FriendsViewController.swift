//
//  FriendsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit
import MagicalRecord

class FriendsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    var friends: [User] = []
    
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(name: "UserTableViewCell")
        
        
        friends = CurrentUser.shared.getFriends()

        SocketManager.shared.emitFriends { (friends, array) in
            
            CurrentUser.shared.saveFriends(friends: array)
            
            self.friends = friends
            self.tableView.reloadData()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.topItem?.title  = "Friends"
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count > 0 {
            
            friends = CurrentUser.shared.getFriends().filter({ (user) -> Bool in
                
                let fullName = (user.firstname ?? "") +  (user.lastname ?? "")
                
                return fullName.lowercased().contains(searchText.lowercased())
            })
            
        }else {
           friends = CurrentUser.shared.getFriends()
            
        }
        tableView.reloadData()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return friends.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.userCell(at: indexPath)
        
        cell.configureCell(user: friends[indexPath.row], type: .sendMessage,  handler: {
            
            
            guard let destinationVC = self.getViewController(name: "ChatViewController"),
                let chatVC = destinationVC as? ChatViewController,
                let navVC = self.getViewController(name: "ChatNavigationController") as? UINavigationController
                else { return }
            
            chatVC.userTo = self.friends[indexPath.row]
            
            navVC.setViewControllers([chatVC], animated: true)
            
            self.present(navVC, animated: true)
        })
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let destinationVC = getViewController(name: "UserProfileViewController"),
            let userVC = destinationVC as? UserProfileViewController
            else { return }
        userVC.user = friends[indexPath.row]
        navigationController?.pushViewController(userVC, animated: true)
    }
    
    
    @IBAction func addFriendsAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "SearchFriendsViewController")
            else { return }
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }

    @IBAction func doneAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
}
