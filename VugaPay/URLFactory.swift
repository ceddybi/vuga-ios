//
//  URLFactory.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/18/17.
//
//

import Foundation

let BASEURL = "https://vn.vugapay.com"

//AUTH
let loginPoint = "/vn/login"
let createAccountPoint = "/vn/reg"
let resetPassPoint = "/vn/passwordreset"
let checkEmailPoint = "/vn/lwe"
let checkPhonePoint = "/vn/lwp"
let generateSMSPoint = "/vn/getcode"

let uploadProfilePhotoPoint = "doc.pic.pro"
let uploadPassportPoint = "doc.pic.id"

let chatPath = "/chat"
let userFetchPoint = "fetch.ios"
//requests
let getAllRequestsPoint = "req.get.all"
//chat
let getAllFriendsPoint = "get.friends"
let searchFriendsPoint = "search.friends"
let addFriendPoint = "friend.add"
let getConvosPoint = "get.convos.all"
let getOnToOnConvos = "get.convos.users"
let sendMessasgePoint = "send.msg.users"

let changeUsernamePoint = "util.chun"
let changePasswordPoint = "util.chpw"

let getAllTransactionsPoint = "pay.get.all.ios"

//payments

let createRequestPoint = "req.create"
let getAllCreditCardsPoint = "cc.get"
let getMomoRatesPoint = "pay.rates.momo"
let getVPRatesPoint = "pay.rates.vp"

let  requestAccept = "req.res"
let  requestReject = "req.rej"

let paycreatePoint = "pay.create"
let addFundsPoint = "cc.pay.ios"
let addCreditCardPoint = "cc.add"

let uploadPhotoCardPoint = "doc.pic.cc"

//utils

let APNPoint = "apn"
let XMINPoint = "xmin"

enum URLFactory {
    //AUTH
    case login()
    case createAccount()
    case resetPass()
    case checkEmail()
    case checkPhone()
    case generateCode()
    //requests
    case getAllRequests()
    //chat
    case getAllFriends()
    
    var path: String {
        
        var url = BASEURL
        
        switch self {
//AUTH
        case .login:
        
            url += loginPoint
            
        case .resetPass:
            
            url += resetPassPoint
            
        case .checkEmail():
            
            url += checkEmailPoint
            
        case .checkPhone():
            
            url += checkPhonePoint
            
        case .generateCode():

            url += generateSMSPoint
//requests
        case .getAllRequests():
            
            url += getAllRequestsPoint
//chat
        case .getAllFriends():
            
            url += getAllFriendsPoint
            
        case .createAccount():
            
            url += createAccountPoint
        }
        
        return url
        
    }
    
    
}
