//
//  ChangeUsernameViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/25/17.
//
//

import UIKit
import SRMModalViewController
import SVProgressHUD

class ChangeUsernameViewController: UIViewController, UITextFieldDelegate {

     @IBOutlet weak var currentUsername: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var usernameTextField: UITextField!
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        self.currentUsername.text = "@" + (CurrentUser.shared.user?.username ?? "")

        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }

    @IBAction func continueAction(_ sender: Any) {
        
        if let pass = CurrentUser.shared.user?.password,
            pass == passwordTextField.text! {
            
            SocketManager.shared.emitChangeUsername(newUsername: usernameTextField.text!,
                                                    pasword: passwordTextField.text!) {
                                                        
                                                        SRMModalViewController.sharedInstance().hide()
                                                        
            }
            
        } else {
            
            SRMModalViewController.sharedInstance().hide()
            
            SVProgressHUD.showError(withStatus: "Wrong password")
        }
        
    }
}
