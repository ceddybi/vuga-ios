//
//  UIViewController.swift
//  Talk2All
//
//  Created by Vladimir Hulchenko on 12/19/16.
//  Copyright © 2016 ClearSoftware. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func getViewController(name: String) -> UIViewController? {
        
        return self.storyboard?.instantiateViewController(withIdentifier: name)
        
    }
}

