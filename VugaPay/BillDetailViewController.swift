//
//  BillDetailViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit
import SRMModalViewController

class BillDetailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var operatorImageView: UIImageView!
    
    var momo: Momo?
    var phoneCode: String = "250"
    var region = "RW"
    var code: String = "250"
    
     var arrayMOMO: [Momo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        PhoneFormatter.setTextFieldPlaceHolder(self.phoneTextField, region: region)
        findXMIN(code: phoneCode)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.phoneTextField {
            
            PhoneFormatter.validateTextfield(textField: textField, region: "RW", range: range, withString: string)
            
            return true
        }
        
        return textField.setNumberCurrencies(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
    }

    @IBAction func tapOnFlag(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "CountryUIViewController"),
            let countries = destinationVC as? CountryUIViewController
            else { return }
        
        var array: [String] = []
        for momo in arrayMOMO {
            
            array.append(momo.iso2!.uppercased())
        }
        
        countries.filerArray = array.sorted()
        
        countries.handlerCountry = { (countryDic, flag) in
            
            self.operatorImageView.image = flag
            
            print("\(countryDic["phone_code"] as! Int)")
            
            self.region = countryDic["country_code"] as! String!
            
            var code = "\(countryDic["phone_code"] as! Int)"
            
            if code.characters.last == "0" {
                
                code = String(code.characters.dropLast(1))
            }
            
            self.phoneCode = code
            PhoneFormatter.setTextFieldPlaceHolder(self.phoneTextField, region: countryDic["country_code"] as! String!)
            
            self.findXMIN(code: "\(countryDic["phone_code"] as! Int)")
            
        }
        
        self.present(countries, animated: true)
        
    }
    
    
    @IBAction func continueAction(_ sender: Any) {
        
        guard let destinationVC =  getViewController(name: "VerifyUserViewController"),
            let verifVC = destinationVC as? VerifyUserViewController
            else { return }
        
        verifVC.handler = {
            
            SRMModalViewController.sharedInstance().hide()
            
            var code = self.code
            
            if code.characters.last == "0" {
                
              code = String(code.characters.dropLast(1))
            }
            
            let params = Airtime.init(phone:("+" + code + self.phoneTextField.text!), amt: self.amountTextField.text!)
            SocketManager.shared.emitCreatePay(type: .airtime(params: params), handler: {
                
                self.navigationController?.dismiss(animated: true)
                
            })
            
            
            
        }
        
        destinationVC.view.frame = CGRect.init(x: 0,
                                               y: 0,
                                               width: 250,
                                               height: 250)
        destinationVC.view.layer.cornerRadius = 10
        
        SRMModalViewController.sharedInstance().showView(with: verifVC)
    }
    
    func findXMIN(code: String) {
        
        for momo in arrayMOMO {
            
            if momo.dialCode == code {
                
                self.momo = momo
                self.code = code

            }
        }
        
    }
}
