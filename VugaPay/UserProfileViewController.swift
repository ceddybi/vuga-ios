//
//  UserProfileViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import UIKit

class UserProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var user: User!
    
    var heightAdd = 44
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addHeaderView()
        checkFriends()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.topItem?.title = (user.firstname ?? "") + " " + (user.lastname ?? "")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //navigationController?.navigationBar.topItem?.title  = ""
    }
    
    func checkFriends() {

        for friend in CurrentUser.shared.getFriends() {
            
            if friend.userid == user.userid {
                
                heightAdd = 0
                tableView.reloadData()
                
            }
        }
    }
    
    func addHeaderView() {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 160))
        
        view.backgroundColor = UIColor.clear
        
        tableView.tableHeaderView = view
        profileImageView.setUserImage(strUrl: user.image_path)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 44
        case 1:
            return CGFloat(heightAdd)
        case 2:
            return 200
        default:
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            
            self.showConfrimAlert(title: "Add to friends?", message: "", handler: { 
                
                SocketManager.shared.emitAddFriend(user: self.user!, handler: {
                    
                    SocketManager.shared.emitFriends(handler: { (_, array) in
                        
                        CurrentUser.shared.saveFriends(friends: array)
                        self.navigationController?.popViewController(animated: true)
                    })
                })
                
            })
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 3
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:

            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath)
            
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath)
            
            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath)
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }

}
