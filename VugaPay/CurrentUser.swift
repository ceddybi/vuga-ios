//
//  CurrentUser.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/18/17.
//
//

import Foundation
import MagicalRecord

enum UserStatus {
    
    case done
    case uploadPassport
    case uploadPassportAgain
    case pending
}


class CurrentUser {
    
    static let shared = CurrentUser()
    
    var user: User?
    
    init() {
        
        self.user = User.mr_findFirst()
        
    }
    
    func logOut() {
        
        MagicalRecord.save({ (context) in
            
            User.mr_truncateAll(in: context)
            Request.mr_truncateAll(in: context)
            Transaction.mr_truncateAll(in: context)
            
            UserDefaults.standard.setValue(nil, forKeyPath: "friends")
            UserDefaults.standard.setValue(nil, forKeyPath: "convos")
            
        }, completion: { _, _ in
            
            SocketManager.shared.disConnectToServer()

        })
        
        
    }
    
    func amount() -> String {
        
        if let user = user {
            
            if let amount = user.counter?.amt_str,
                let amountDouble = Double(amount){
             
                let amount = amountDouble/100
                
                let str = "$" + "\(amount)"
                if str.characters.count < 3 {
                    return "$0.00"
                }
                
                return str
            }
            
        }
        return "$0.00"
    }
    
    
    func userId() -> String {
        
        //return "vt"
        
        if let user = user {
            
            return user.userid!
        }
        
        return ""
        
    }
    
    func saveFriends(friends: [[String: Any]]) {
    
        self.saveData(array: friends, key: "friends")
    }
    
    func saveData(array: [[String: Any]], key: String) {
        
        if let json = try? JSONSerialization.data(withJSONObject: ["data":array], options: []) {
            if let content = String(data: json, encoding: .utf8) {
                // here `content` is the JSON dictionary containing the String
                UserDefaults.standard.setValue(content, forKeyPath: key)
            }
        }
    }
    
    func getFriends() -> [User] {
    
        if let users = UserDefaults.standard.value(forKey: "friends") as? String,
            let array = convertToDictionary(text: users){
            
            return User.mr_import(from: array) as! [User]
        }
        
        return []
    }
    
    func saveConvos(friends: [[String: Any]]) {
        self.saveData(array: friends, key: "convos")

    }
    
    func getConvos() -> [Convos] {
        
        if let users = UserDefaults.standard.value(forKey: "convos") as? String,
            let array = convertToDictionary(text: users){
            
            return Convos.mr_import(from: array) as! [Convos]
        }
        
        return []
    }
    
    func convertToDictionary(text: String) -> [[AnyHashable: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                let json =  try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: Any]
                
                return (json?["data"] as! [[AnyHashable: Any]])
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func userStatus() -> UserStatus{
        
        //return .done
        
        if let user = user,
            user.isVerifyByAdmin == 1 {
            return .done
        }
        
        if let status = user?.status {
            
            if status == "" {
                
                return .uploadPassport
            }
            
            if status == "fail" {
                
                return .uploadPassportAgain
            }
            
            if status == "pend" {
                
                return .pending
            }
            
            if status == "done" {
                
                return .done
            }
            
        }
        return .uploadPassport
    }
    
    func setPassword(password: String) {
        
        UserDefaults.standard.set(password, forKey: "password")
        
    }
    
    func getPassword() -> String {
        
       return UserDefaults.standard.value(forKey: "password") as! String
        
    }
    
}
