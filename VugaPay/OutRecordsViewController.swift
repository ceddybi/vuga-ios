//
//  OutRecordsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit
import SVProgressHUD

class OutRecordsViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var transactionOut: [Transaction] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(name: "TransactionTableViewCell")
        self.filterTransaction(transactions: Transaction.mr_findAll() as! [Transaction])
        SocketManager.shared.emitAllTransactions { transactions in
            
            self.filterTransaction(transactions: transactions)
            
        }
    }
    
    func filterTransaction(transactions: [Transaction]) {
        
        var array: [Transaction] = []
        
        for transaction in transactions {
            
            if transaction.from?.userid == CurrentUser.shared.userId() {
                
                array.append(transaction)
            }
            
        }
        self.transactionOut = array
        self.tableView.reloadData()
    }

    @IBAction func doneAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return transactionOut.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.transactionCell(at: indexPath)
        
        cell.configure(with: transactionOut[indexPath.row])
        
        return cell
        
    }
}
