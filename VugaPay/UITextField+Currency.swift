//
//  UITextField+Currency.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/31/17.
//
//

import Foundation
import UIKit

extension UITextField {
    
    func setNumberCurrencies(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        textField.text = textField.text?.replacingOccurrences(of: "$", with: "")
        
        guard let str = textField.text else {
            textField.text = "0.00"
            return false
        }
        
        let value = (str as NSString).doubleValue
        
        var cents = round(value * 100)
        if string.characters.count > 0 {
            for c in string.characters {
                if let num = Int(String(c)) {
                    cents *= 10
                    cents += Double(num)
                }
            }
        }
        else {
            cents = floor(cents / 10)
        }
        
        textField.text = NSString(format: "$%.2f", cents/100) as String
        
        return false
    }

    
}
