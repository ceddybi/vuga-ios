//
//  MainViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/11/17.
//
//

import UIKit
import QRCodeReaderViewController
import SRMModalViewController

extension MainViewController {
    
    func countOfRequests(requests: [Request]) ->Int {
        
        var count = 0
        
        for req in requests {
            
            if req.status == "pend" {
                
                count = count + 1
            }
        }
        
        return count
    }
    
    func countOfTransactions(transactions: [Transaction])->Int {
        
        var count = 0
        
        for tran in transactions {
            
            if tran.status == "pend" {
                
                count = count + 1
            }
        }
        return count
    }
}

class MainViewController: UIViewController, QRCodeReaderDelegate {

    @IBOutlet weak var amountUserLabel: UILabel!
    @IBOutlet weak var chatCountLabel: UILabel!
    @IBOutlet weak var requestsCountLabel: UILabel!
    @IBOutlet weak var recordsCountLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        methodOfReceivedNotification()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.methodOfReceivedNotification), name: NSNotification.Name(rawValue: "update"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector:  #selector(self.showAlert(_:)), name: NSNotification.Name(rawValue: "show"), object: nil)
    }
    
    func methodOfReceivedNotification() {
        
        if let path = CurrentUser.shared.user?.image_path,
            path.characters.count != 0 {
            
        } else {
            
            guard let destinationVC = self.getViewController(name: "ProfilePhotoViewController")
                else { return}
            
            self.present(destinationVC, animated: true)
            
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.registerForPushNotifications(application: UIApplication.shared)
        
        amountUserLabel.text = CurrentUser.shared.amount()
        if let countChat = CurrentUser.shared.user?.chat_int {

            chatCountLabel.text = "\(countChat)"
        }
        
        self.requestsCountLabel.text = "\(self.countOfRequests(requests: Request.mr_findAll() as! [Request]))"
        
        SocketManager.shared.emitRequests { requests in
            
            self.requestsCountLabel.text = "\(self.countOfRequests(requests: requests))"
            
        }
        
        self.recordsCountLabel.text = "\(self.countOfTransactions(transactions: Transaction.mr_findAll() as! [Transaction]))"
        
        SocketManager.shared.emitAllTransactions { transactions in
            
            self.recordsCountLabel.text = "\(self.countOfTransactions(transactions: transactions))"
        }
        
    }
    
    func showAlert(_ notification: NSNotification) {
        
        var url = "itms-apps://itunes.com"
        
        if let link = notification.userInfo?["link"] as? String {
            url = link
        }
        
        showUpdateAlert(link: url, title: "Please update this app to get new features", message: "There is a new version available")
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SocketManager.shared.emitFetchUser {
            
        }
    }
    
    @IBAction func massegesAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "MessagesTabBarController") as? UITabBarController
            else { return }
        
        destinationVC.selectedIndex = 1
        
        self.present(destinationVC, animated: true)
    }

    @IBAction func addFriendsAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "SearchFriendsNavigationController")
            else { return }
        
        self.present(destinationVC, animated: true)
    }
    
    @IBAction func qrCodeAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "QRViewController"),
        let qrViewController = destinationVC as? QRViewController
            else { return }
        
        qrViewController.scanHandler = {
            
            SRMModalViewController.sharedInstance().hide()

            let reader = QRCodeReader.init(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            
            let readerVC = QRCodeReaderViewController.init(cancelButtonTitle: "Cancel",
                                                           codeReader: reader,
                                                           startScanningAtLoad: true,
                                                           showSwitchCameraButton: true,
                                                           showTorchButton: true)
            
            readerVC.modalPresentationStyle = .formSheet
            readerVC.delegate = self
            readerVC.setCompletionWith { (result) in
                
                if let result = result {
                    
                    SocketManager.shared.emitSearchFriends(query: result) { (users) in
                        
                        readerVC.dismiss(animated: false)
                        
                        SocketManager.shared.emitXMIN(handler: { _, bank in
                            
                            guard let navVC = self.getViewController(name: "SendMoneyQRNavigationController") as? UINavigationController,
                                let requestVC = navVC.viewControllers.first as? SendToUserViewController
                                else { return }
                            
                            requestVC.user = users.first
                            requestVC.bankAmount = bank
                            requestVC.isModally = true
                            
                            self.present(navVC, animated: true)

                        })

                        
                     
                    }
                    
                }
                
                print(result ?? "error")
            }
            
            self.present(readerVC, animated: true)
            
        }
        
        destinationVC.view.frame = CGRect.init(x: 0,
                                               y: 0,
                                               width: 250,
                                               height: 300)
        destinationVC.view.layer.cornerRadius = 10
        SRMModalViewController.sharedInstance().showView(with: destinationVC)
        
    }
    
    @IBAction func logOutAction(_ sender: Any) {
        
        self.showConfrimAlert(title: "Are you sure want to logout?", message: "") { 
           
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.loginFlow()
            
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        
    }
    
    @IBAction func sendMoneyAction(_ sender: Any) {
        
        if allowTransaction() {
            guard let destinationVC = getViewController(name: "SendMoneyNavigationController")
                else { return }
            
            self.present(destinationVC, animated: true)
        }

    }
    
    @IBAction func requestsAction(_ sender: Any) {
        
        if allowTransaction() {
        
        guard let destinationVC = getViewController(name: "RequestsNavigationController")
            else { return }
        
        self.present(destinationVC, animated: true)
        }
    }
    
    @IBAction func payBillsAction(_ sender: Any) {
        if allowTransaction() {
        guard let destinationVC = getViewController(name: "PayBillsNavigationController")
            else { return }
        
        self.present(destinationVC, animated: true)
        }
    }
    
    @IBAction func addFundsAction(_ sender: Any) {

        if allowTransaction() {
        guard let destinationVC = getViewController(name: "AddFundsNavigationController")
            else { return }
        
        self.present(destinationVC, animated: true)
        }
    }
    
    @IBAction func recordsAction(_ sender: Any) {
        if allowTransaction() {
        guard let destinationVC = getViewController(name: "RecordsTabBarController")
            else { return }
        
        self.present(destinationVC, animated: true)
        }

    }
    
    @IBAction func loansAction(_ sender: Any) {
        
        self.showOkAlert(title: "This service is not available", message: "")
    }
    
    public func readerDidCancel(_ reader: QRCodeReaderViewController!) {
        
        reader.dismiss(animated: true)
    }
}
