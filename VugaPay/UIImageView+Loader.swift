//
//  UIImageView+Loader.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    
    func setUserImage(strUrl: String?) {
        
        guard let path = strUrl,
            let url = URL.init(string: path)
            else { return }
        
        self.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "user_empty"), options: [.progressiveDownload, .highPriority])
    }
    
    func setUserImageCell(strUrl: String?, handler: @escaping (UIImage?) -> Void) {
        
        guard let path = strUrl,
            let url = URL.init(string: path)
            else { return }
        
        self.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "user_empty"), options: [.progressiveDownload, .highPriority]) { (image, _, _, _) in
            handler(image)
        }
    }
    
    
    
}
