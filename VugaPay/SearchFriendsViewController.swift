//
//  SearchFriendsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/11/17.
//
//

import UIKit

class SearchFriendsViewController: UIViewController, UITableViewDataSource, UISearchBarDelegate, UITableViewDelegate {
    
    var users: [User] = []

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(name: "UserTableViewCell")
        
       searchUsers()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.topItem?.title  = "Search Friends"
    }
    
    func searchUsers(){
        
        SocketManager.shared.emitSearchFriends(query: searchBar.text!) { (users) in
            
            self.users = users
            
            self.tableView.reloadData()
            
        }
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        
        searchUsers()
    }
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        self.view.endEditing(true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return users.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.userCell(at: indexPath)
            
        cell.configureCell(user: users[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        guard let destinationVC = getViewController(name: "UserProfileViewController"),
            let userVC = destinationVC as? UserProfileViewController
            else { return }
        userVC.user = users[indexPath.row]
        navigationController?.pushViewController(userVC, animated: true)
        
    }

    @IBAction func doneAction(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
}
