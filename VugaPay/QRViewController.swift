//
//  QRViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 2/1/17.
//
//

import UIKit
import SRMModalViewController
import QRCode

class QRViewController: UIViewController {

    @IBOutlet weak var QRImageView: UIImageView!
    
    var scanHandler: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        if let user = CurrentUser.shared.user {
            
            let qrCode = QRCode((user.phone ?? ""))
            
            self.QRImageView.image = qrCode?.image
            
        }
    }

    @IBAction func scanAction(_ sender: Any) {
       
        if let scanHandler = scanHandler {
            
            scanHandler()
        }
        
        
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        SRMModalViewController.sharedInstance().hide()
        
    }
}
