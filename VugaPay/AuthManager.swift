//
//  AuthManager.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/18/17.
//
//

import Foundation
import Alamofire
import SVProgressHUD
import MagicalRecord
import CoreData

public extension UIImage
{
    func base64Encode() -> String?
    {
        guard let imageData = UIImageJPEGRepresentation(self, 0.1) else
        {
            return nil
        }
        
        let base64String = imageData.base64EncodedString()
        
        return base64String
    }
}

class HandlerError {
    
    static func checkServerError(response: DataResponse<Any>) -> Bool {
        
        if response.result.isSuccess,
            let data = response.result.value as? [Any],
            let success = data.first as? String,
            success != "Success"{
            
            
            SVProgressHUD.showError(withStatus: "\(data)")
            return true
        }
        
        if response.result.isSuccess,
            let value = response.result.value as? [String: String],
            let error = value["error"]{
            
            
            SVProgressHUD.showError(withStatus: error)
            return true
        }
        
        return false
    }
    
}

class AuthManager {
    
    static func login(login: String, password: String, handler: @escaping (() -> Void) ) {

        let url = URLFactory.login()
        
        var params: [String: String] = [:]
        
        params = ["username": login]
        
        params["password"] = password
        
        SVProgressHUD.show(withStatus: "Login")
        Alamofire.request(url.path, method: .post, parameters: params).responseJSON { (response) in
            
            SVProgressHUD.dismiss()
            print(response)
            if HandlerError.checkServerError(response: response) {
                return
            }
            if response.result.isSuccess,
                var json = response.result.value as? [AnyHashable: Any]{
                
                json["password"] = password
                CurrentUser.shared.setPassword(password: password)
                
                MagicalRecord.save({ (context) in
                    
                    let _ = User.mr_import(from: (json as! Any), in: context)
                    
                }, completion: { (_, _) in
                    
                    CurrentUser.shared.user = User.mr_findFirst()
                    
                    handler()
                    
                })
                
                
            } else {
                
                SVProgressHUD.showError(withStatus: "Error login")
            }
        }
    }
    
    static func createAccount(username: String,
                              firstname: String,
                              lastname: String,
                              phone: String,
                              email: String,
                              password: String, handler: @escaping (() -> Void) ) {
        
        let url = URLFactory.createAccount()
        
        let params: [String: String] = ["phone": phone,
                                        "email":email,
                                        "firstname": firstname,
                                        "lastname": lastname,
                                        "username": username,
                                        "password": password]
        
        SVProgressHUD.show(withStatus: "Sign Up")
        Alamofire.request(url.path, method: .post, parameters: params).responseJSON { (response) in
            
            SVProgressHUD.dismiss()
            print(response)
            if HandlerError.checkServerError(response: response) {
                return
            }
            if response.result.isSuccess,
                var json = response.result.value as? [AnyHashable: Any]{
                
                json["password"] = password
                
                CurrentUser.shared.setPassword(password: password)
                
                MagicalRecord.save({ (context) in
                    
                    let user = User.mr_import(from: json, in: context)
                    
                    
                }, completion: { (_, _) in
                    
                    CurrentUser.shared.user = User.mr_findFirst()
                    
                    handler()
                    
                })
                
                
            } else {
                
                SVProgressHUD.showError(withStatus: "Error registration")
            }
        }
    }
    
    static func resetPassword(email: String, handler: (() -> Void) ) {
    
        let url = URLFactory.login()
    
        SVProgressHUD.show(withStatus: "Instructions sending to email")
        Alamofire.request(url.path, method: .post, parameters: ["email":email]).responseJSON { (response) in
    
            SVProgressHUD.dismiss()
            print(response)
            if HandlerError.checkServerError(response: response) {
                return
            }
            if response.result.isSuccess {
    
    
            } else {
    
                SVProgressHUD.showError(withStatus: "Error sent")
            }
            
        }
    }
    
    static func checkEmail(email: String, handler: @escaping (() -> Void) ) {
        
        let url = URLFactory.checkEmail()
        
        SVProgressHUD.show(withStatus: "Checking")
        Alamofire.request(url.path, method: .post, parameters: ["email":email]).responseJSON { (response) in
            
            SVProgressHUD.dismiss()
            print(response)
            if HandlerError.checkServerError(response: response) {
                return
            }
            if response.result.isSuccess {
                
                 handler()
                
            } else {
                
                SVProgressHUD.showError(withStatus: "Error")
            }
            
        }
    }
    
    static func checkPhone(phone: String, code: UInt64, handler: @escaping (() -> Void) ) {
        
        let url = URLFactory.checkPhone()
        
        SVProgressHUD.show(withStatus: "Checking")
        Alamofire.request(url.path, method: .post, parameters: ["phone":phone, "code": "\(code)"]).responseJSON { (response) in
            
            SVProgressHUD.dismiss()
            print(response)
            if HandlerError.checkServerError(response: response) {
                return
            }
            
            if response.result.isSuccess {
                
                handler()
                
            } else {
                
                SVProgressHUD.showError(withStatus: "Error")
            }
        }
    }
    
    static func generateSMSCode(phone: String, code: UInt64, handler: @escaping ((UInt64) -> Void) ) {
        
        let url = URLFactory.generateCode()
        
        SVProgressHUD.show(withStatus: "Sending SMS code")
        
        let params = ["phone": phone, "code": "\(code)"] as [String : Any]
        
        Alamofire.request(url.path, method: .post, parameters: params, encoding: JSONEncoding.default).responseString { (response) in
            
            SVProgressHUD.dismiss()
            print(response)
            
            if response.result.isSuccess,
                let value = response.result.value,
                value == " Success"{
                
                handler(code)
                
            } else {
                
                SVProgressHUD.showError(withStatus: "Error")
            }
            
        }
    }

    static func randomNumber() -> UInt64
    {
        return UInt64(arc4random_uniform(99999) + 10000)
    }
    
}

//static func login(login: String, password: String, handler: (() -> Void) ) {
//    
//    let url = URLFactory.login()
//    
//    SVProgressHUD.show(withStatus: "Login")
//    Alamofire.request(url.path, method: .get, parameters: ["":""]).responseJSON { (response) in
//        
//        SVProgressHUD.dismiss()
//        print(response)
//        
//        if response.result.isSuccess {
//            
//            
//        } else {
//            
//            SVProgressHUD.showError(withStatus: "Error login")
//        }
//        
//    }
//}
