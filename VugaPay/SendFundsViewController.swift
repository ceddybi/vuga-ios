//
//  SendFundsViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/26/17.
//
//

import UIKit

class SendFundsViewController: UIViewController, UITextFieldDelegate {
    
    var card: Cards!

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == passwordTextField {
            
            return true
        }
        
        return textField.setNumberCurrencies(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }

    @IBAction func sendAction(_ sender: Any) {
        
        var text = amountTextField.text!.replacingOccurrences(of: "$", with: "")
         text = text.replacingOccurrences(of: ".", with: "")
        
        if CurrentUser.shared.getPassword() == passwordTextField.text{
            
            SocketManager.shared.emitAddFunds(cid: card.cid!, amt: text) {
                
                self.navigationController?.popViewController(animated: true)
            }
            
        } else {
            
            showOkAlert(title: "Wrong password", message: "")
            
        }
        
    }
}
