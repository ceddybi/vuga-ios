//
//  SendMobileMoneyViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/16/17.
//
//

import UIKit
import MagicalRecord

struct BankAmount {
    var max: UInt64
    var min: UInt64
}

class SendMobileMoneyViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var opertaorImageView: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    var momo: Momo?
    var phoneCode: String = "250"
    var region = "RW"
    var code: String?
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    
    var arrayMOMO: [Momo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
         PhoneFormatter.setTextFieldPlaceHolder(self.phoneTextField, region: region)
         findXMIN(code: phoneCode)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == amountTextField {
           
            
            if let _ = momo {
                
                return true
                
            } else {
                
                showErrorNotAvailableCountry()
                
                return false
            }
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.phoneTextField {
            
            PhoneFormatter.validateTextfield(textField: textField, region: region, range: range, withString: string)
        }
        
        if textField == self.amountTextField {
            
            return textField.setNumberCurrencies(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        
        return true
        
    }

    @IBAction func continueAction(_ sender: Any) {
        
        if let momo = momo {
            
            var text = self.amountTextField.text!.replacingOccurrences(of: "$", with: "")
            text = text.replacingOccurrences(of: ".", with: "")
            
            if let intCount = Int64(text),
                intCount > momo.min, intCount < momo.max{
                
                //emitMomoRates
                
                if PhoneFormatter.validatePhone(phone: self.phoneTextField.text!, region: momo.iso2!) {
                    
                    SocketManager.shared.emitMomoRates(amt: intCount, country2letters: momo.iso2!, handler: { (rate) in
                        
                        print(rate)
                        
                        guard let destinationVC = self.getViewController(name: "ConfirmTransactionViewController"),
                            let confirmVC = destinationVC as? ConfirmTransactionViewController
                            else { return }
                        
                        confirmVC.typeTransaction = .sendPhoneMoney
                        
                        self.navigationController?.pushViewController(confirmVC, animated: true)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            
                            
                            var code = self.code!
                            
                            if code.characters.last == "0" {
                                
                                code = String(code.characters.dropLast(1))
                            }
                            
                            confirmVC.configurePhoneTransaction(rate: rate, phone: code + self.phoneTextField.text!, countryCode: momo.iso2!, flag: self.flagImageView.image!)
                        }
                        
                    })
                } else {
                    
                    self.showOkAlert(title: "Wrong phone number", message: "")
                }
                
            } else {
                
                self.showOkAlert(title: "Please enter current amount", message: "")
                
            }
         
        } else {
            
            showErrorNotAvailableCountry()
            
        }
    }
    
    
    @IBAction func tapOnFlag(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "CountryUIViewController"),
            let countries = destinationVC as? CountryUIViewController
            else { return }
        
        var array: [String] = []
        for momo in arrayMOMO {
            
            array.append(momo.iso2!.uppercased())
        }
        
        countries.filerArray = array.sorted()
        
        countries.handlerCountry = { (countryDic, flag) in
            
            self.flagImageView.image = flag
            
            print("\(countryDic["phone_code"] as! Int)")
            
            self.region = countryDic["country_code"] as! String!
            
            var code = "\(countryDic["phone_code"] as! Int)"
            
            if code.characters.last == "0" {
                
                code = String(code.characters.dropLast(1))
            }
            
            self.phoneCode = code
            PhoneFormatter.setTextFieldPlaceHolder(self.phoneTextField, region: countryDic["country_code"] as! String!)
            
            self.findXMIN(code: "\(Int(code)!)")

        }
        
        self.present(countries, animated: true)
        
    }
    
    func showErrorNotAvailableCountry() {
        
        var contries = ""
        
        for xmin in arrayMOMO {
            
            contries += " " + (xmin.name ?? "")
        }
        
        self.showOkAlert(title: "Choose valid country", message: contries)
        
    }
    
    func findXMIN(code: String) {
        
        for momo in arrayMOMO {
            
            if momo.dialCode == code {
                
                self.momo = momo
                self.code = code
                
                self.opertaorImageView.text = momo.tele
            }
        }
        
    }
}
