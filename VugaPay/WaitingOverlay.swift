//
//  WaitingOverlay.swift
//  BoardTime
//
//  Created by Полар Групп on 15.08.16.
//  Copyright © 2016 Polar Development Group. All rights reserved.
//

import Foundation
import UIKit


open class WaitingOverlay{
    
    var overlayView = UIView()
    var messageFrame = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var label = UILabel()
    //var rootView = UIView?
    static let sharedInstance = WaitingOverlay()
    
    /*class var shared: WaitingOverlay {
        struct Static {
            static let instance: WaitingOverlay = WaitingOverlay()
        }
        return Static.instance
    }*/
    
    open func showOverlay(_ view: UIView!) {
        //rootView = view
        let marginLeft: CGFloat = 5
        overlayView = UIView(frame: UIScreen.main.bounds)
        overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let overlayWidth = 270
        messageFrame = UIView(frame: CGRect(x: 0, y: 0 , width: overlayWidth, height: 50))
        
        //message frame centered
        messageFrame.center = CGPoint(x: overlayView.bounds.midX, y: overlayView.bounds.midY)
        messageFrame.autoresizingMask = [.flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleTopMargin]
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator.center = CGPoint(x: messageFrame.bounds.minX + (activityIndicator.frame.midX + marginLeft), y: messageFrame.bounds.midY)
        
        let width = 220
        
        let center = Int(activityIndicator.frame.width) + 5
        label = UILabel(frame: CGRect(x: center, y: 0, width: width, height: 50))
        label.text = "Loading data, wait..."

        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
  
        messageFrame.addSubview(label)
        messageFrame.addSubview(activityIndicator)

        
        overlayView.addSubview(messageFrame)
        activityIndicator.startAnimating()
        view.addSubview(overlayView)
    }
    
    open func hideOverlayView() {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
        //return rootView
    }
}
