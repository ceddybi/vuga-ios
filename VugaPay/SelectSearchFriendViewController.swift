//
//  SelectSearchFriendViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/23/17.
//
//

import UIKit

enum TypeSearch {
    case base
    case sendMoney
    case sendRequest
}

class SelectSearchFriendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var users: [User] = []
    
    var typeSearch = TypeSearch.base
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerCell(name: "UserTableViewCell")

        SocketManager.shared.emitFriends { (friends, _) in
            
            self.users = friends
            self.tableView.reloadData()
            
        }
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        SocketManager.shared.emitSearchFriends(query: searchBar.text!) { (users) in
            
            self.users = users
            
            self.tableView.reloadData()
            
        }
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.view.endEditing(true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return users.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.userCell(at: indexPath)
        
        cell.configureCell(user: users[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch typeSearch {
        case .base:
            
            break
            
        case .sendRequest:
            
            guard let destinationVC = self.getViewController(name: "SendToUserViewController"),
                let requestVC = destinationVC as? SendToUserViewController
                else { return }
            
            requestVC.user = self.users[indexPath.row]
            
            self.navigationController?.pushViewController(requestVC, animated: true)
            
            break
        case .sendMoney:
            
            SocketManager.shared.emitXMIN(handler: { _, bank in
                
                guard let destinationVC = self.getViewController(name: "SendToUserViewController"),
                    let requestVC = destinationVC as? SendToUserViewController
                    else { return }
                
                requestVC.user = self.users[indexPath.row]
                requestVC.bankAmount = bank
                
                self.navigationController?.pushViewController(destinationVC, animated: true)
            })
            
            
            break
        }
        
    }


}
