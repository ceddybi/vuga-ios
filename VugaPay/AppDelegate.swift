//
//  AppDelegate.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/9/17.
//
//

import UIKit
import CoreData
import Fabric
import Crashlytics
import MagicalRecord
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().tintColor = UIColor(red: 46/255, green: 102/255, blue: 90/255, alpha: 1)
        
        MagicalRecord.enableShorthandMethods()
        MagicalRecord.setupCoreDataStack(withStoreNamed: "VugaPay")
        
        if let _ = CurrentUser.shared.user {
            
            mainFlow()
            
        }
        
        SocketManager.shared
        CurrentUser.shared
        
        return true
    }
    
    func loginFlow() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let windowAppDelegate = window{
            
            CurrentUser.shared.logOut()
            SocketManager.shared.disConnectToServer()
                
            windowAppDelegate.rootViewController = storyboard.instantiateViewController(withIdentifier: "EnterNavigationController")
            
        }
    }
    
    func mainFlow() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let windowAppDelegate = window{
            
            SocketManager.shared.connectToServer()
            
            windowAppDelegate.rootViewController = storyboard.instantiateViewController(withIdentifier: "MainTabBarController")
            
        }
    }
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

    }
    
    func tokenRefreshNotificaiton(notification: NSNotification) {

    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenString = deviceToken.base64EncodedString()
        print("Device Token:", tokenString )
        SocketManager.shared.emitAPN(token: tokenString)
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       willPresent notification: UNNotification,
                                       withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
        
    }
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       didReceive response: UNNotificationResponse,
                                       withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("Message ID: \(userInfo["gcm.message_id"])")
        
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Message ID: \(userInfo["gcm.message_id"])")
        
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        if let _ = CurrentUser.shared.user {
            
            SocketManager.shared.disConnectToServer()
            
        }

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if let _ = CurrentUser.shared.user {
            
            SocketManager.shared.connectToServer()
            
        }
    }
}

