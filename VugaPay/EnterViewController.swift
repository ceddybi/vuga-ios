//
//  EnterViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/9/17.
//
//

import UIKit

class EnterViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var imagePoint = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signUpButton.roundWhiteButton()
        loginButton.roundWhiteButton()
        runAnimatingColors()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColor(red: 46/255, green: 102/255, blue: 90/255, alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor(red: 46/255, green: 102/255, blue: 90/255, alpha: 1)]
    }
    
    func runAnimatingColors() {
        
        let arrayImages = [#imageLiteral(resourceName: "1_img"), #imageLiteral(resourceName: "2_img"), #imageLiteral(resourceName: "3_img"), #imageLiteral(resourceName: "4_img"), #imageLiteral(resourceName: "5_img")]
        
        UIView.transition(with: imageView, duration: 3, options: .transitionCrossDissolve, animations: {
            self.imageView.image = arrayImages[self.imagePoint]
        }) { (_) in
            
            if self.imagePoint == arrayImages.count - 1 {
                
                self.imagePoint = -1
                
            }
            
            self.imagePoint = self.imagePoint + 1
            
            self.perform(#selector(EnterViewController.runAnimatingColors), with: nil, afterDelay: 1)
        }
    }

    @IBAction func signUpAction(_ sender: Any) {
      
        guard let destinationVC = getViewController(name: "RegistrationViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
      
        guard let destinationVC = getViewController(name: "LoginViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
    }
}
