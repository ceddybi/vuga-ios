//
//  AddFundsTableViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit

class AddFundsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
            
        case 0:
            
            if indexPath.row == 0{
                
                guard let destinationVC = getViewController(name: "CardsViewController")
                    else { return }
                
                navigationController?.show(destinationVC, sender: self)
                
            }
            if indexPath.row == 1{
                
                self.showOkAlert(title: "Not available", message: "")
                
            }
            if indexPath.row == 2{
                
                self.showOkAlert(title: "Not available", message: "")
                
            }
            
            
            break
        case 1:
            
            guard let destinationVC = getViewController(name: "HelpViewController") as? HelpViewController
                else { return }
            
            destinationVC.urlStr = "http://docs.vugapay.com/why-should-i-use-vugapay"
            
            navigationController?.show(destinationVC, sender: self)
            
            break
        default:
            break
        }
        
    }
    
    @IBAction func doneAaction(_ sender: Any) {
        
         navigationController?.dismiss(animated: true)
    }
    
}
