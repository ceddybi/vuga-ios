//
//  Payments.swift
//  VugaTest
//
//  Created by Полар Групп on 11.03.17.
//  Copyright © 2017 Полар Групп. All rights reserved.
//

import UIKit
//import Alamofire
import SocketIO

enum PaymentAction: String {
    case Charged = "vp"
    case Paid = "req"
}

class PaymentCell: UITableViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var date: UILabel!
    
}

struct History {
    var date: Date
    
    var type: PaymentAction
    
    var sender: String
    var senderImage: String
    
    var recipient: String
    var recipientImage: String
    
}

class Payments: UITableViewController {
    var socket: SocketIOClient!

    var items: [History] = []

    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.tableView.contentInset = UIEdgeInsets(top: 40,left: 0,bottom: 0,right: 0)
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    
        //let url = URLFactory.getHistory()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()     
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        WaitingOverlay.sharedInstance.showOverlay(self.view)
        
        items = SocketManager.shared.source
        items.reverse()
        
        self.tableView.reloadData()
        
        WaitingOverlay.sharedInstance.hideOverlayView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! PaymentCell

        // Configure the cell...
        
        let currentItem = items[indexPath.row]
        
        cell.view = prepareCellLayer(view: cell.view)
        
        cell.avatar = prepareAvatar(image: cell.avatar)
        
        let actionType = " \(currentItem.type) "
        
        cell.label.attributedText = prepareLabel(sender: currentItem.sender, recipient: currentItem.recipient, type: actionType)
        
        cell.date.text = prepareDate(date: currentItem.date)
        
        cell.avatar.image = UIImage.contentOfURL(link: currentItem.senderImage)

        return cell
    }
 
    func stringToBold(string: String) -> NSAttributedString {
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 20)]
        let boldString = NSMutableAttributedString(string:string, attributes:attrs)
        
        return boldString
    }
    
    func prepareDate(date: Date) -> String {
        
        let elapsed = Int(Date().timeIntervalSince(date))
        
        switch elapsed {
        case 0..<60:
            return "\(elapsed % 60) seconds ago"
        
        case 60..<3600:
            return "\((elapsed / 60) % 60) minutes ago"
            
        default:
            return "\((elapsed / 3600)) hours ago"
        }

    }
    
    func prepareLabel(sender: String, recipient: String, type: String) -> NSAttributedString {
        let actionType = NSAttributedString.init(string: type)
        
        let tmpString: NSMutableAttributedString = stringToBold(string: sender) as! NSMutableAttributedString
        tmpString.append(actionType)
        tmpString.append(stringToBold(string: recipient))
        
        return tmpString
    }
    
    func prepareCellLayer(view: UIView) -> UIView {
        view.layer.cornerRadius = 5
        
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        
        view.layer.shadowOffset = CGSize.init(width: 0, height: 2)
        //cell.view.layer.shadowColor = UIColor.black.cgColor
        //cell.view.layer.shadowRadius = 5
        view.layer.masksToBounds = false
        view.layer.shadowOpacity = 0.5
        //cell.view.layer.shouldRasterize = true

        
        return view
    }
    
    func prepareAvatar(image: UIImageView) -> UIImageView {
        image.layer.borderWidth = 0
        image.layer.masksToBounds = false
        image.layer.borderColor = UIColor.clear.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
        
        return image
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImage {
    class  func contentOfURL(link: String) -> UIImage {
        let url = URL.init(string: link)!
        var image = UIImage()
        do{
            let data = try Data.init(contentsOf: url)
            image = UIImage.init(data: data)!
        } catch _ {
            print("error downloading images")
        }
        return image
    }
}
