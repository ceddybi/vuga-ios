//
//  SendToTableViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit

class SendToTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func doneAction(_ sender: Any) {
        
        navigationController?.dismiss(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
        case 0:
            
            if indexPath.row == 0 {
                
                
                guard let destinationVC = getViewController(name: "SelectSearchFriendViewController"),
                let searchVC = destinationVC as? SelectSearchFriendViewController
                    else { return }
                
                searchVC.typeSearch = .sendMoney
                
                navigationController?.pushViewController(destinationVC, animated: true)
                
            }
            
            if indexPath.row == 1 {
                
                SocketManager.shared.emitXMIN(handler: { MOMOS, _ in
                  
                    guard let destinationVC = self.getViewController(name: "SendMobileMoneyViewController"),
                    let mobileMoneyVC = destinationVC as? SendMobileMoneyViewController
                        else { return }
                    
                    mobileMoneyVC.arrayMOMO = MOMOS
                    
                    self.navigationController?.pushViewController(mobileMoneyVC, animated: true)
                })
                
            }
            
            if indexPath.row == 2 {
                
                  self.showOkAlert(title: "Not available", message: "")
                
            }
            break
            
        case 1:
            
            if indexPath.row == 0 {
                
                guard let destinationVC = getViewController(name: "HelpViewController") as? HelpViewController
                    else { return }
                
                destinationVC.urlStr = "http://docs.vugapay.com/vugapay-cross-border-rates"
                
                navigationController?.show(destinationVC, sender: self)
                
            }
            
            if indexPath.row == 1 {
                
                guard let destinationVC = getViewController(name: "HelpViewController") as? HelpViewController
                    else { return }
                
                destinationVC.urlStr = "http://docs.vugapay.com/how-to-send-money"
                
                 navigationController?.show(destinationVC, sender: self)
                
            }
            
            break
        default:
            break
        }
        
    }

}
