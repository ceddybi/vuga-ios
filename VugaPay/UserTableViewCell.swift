//
//  UserTableViewCell.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import UIKit

enum TypeButton {
    case sendMessage
    case conversetion(lastList: String?)
    case none
    
    var lastMessage: String {
        
        switch self {
        case .conversetion(let lastList):
            
            if let last = lastList {
                
                return last
            }
            return ""
        default:
            return ""
        }
    }
}

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    var handlerChat: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(user: User, type: TypeButton = .none, handler: (() -> Void)? = .none) {
        
        handlerChat = handler
        
        nameLabel.text = (user.firstname ?? "") + " " + (user.lastname ?? "")
        
        userNameLabel.text = "@" + (user.username ?? "")
        profileImage.image = #imageLiteral(resourceName: "user_empty")
        
        profileImage.setUserImageCell(strUrl: user.image_path) { (image) in
            if let img = image {
               self.profileImage.image = img
            }
        }
        print(user.image_path)
        
        
        
        switch type {
        case .sendMessage:
            
            actionButton.isHidden = false
            
            break
            
        case .conversetion:
            
            userNameLabel.text = type.lastMessage
            
            break
        default:
            break
        }
        
    }
    
    @IBAction func actionButton(_ sender: Any) {
        
        
        if let handlerChat = handlerChat {
            
            handlerChat()
        }
        
    }
}
