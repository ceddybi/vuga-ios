//
//  HelpViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/11/17.
//
//

import UIKit

class HelpViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var activiryIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    
    var urlStr = "http://docs.vugapay.com/"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let urlRequest = URLRequest.init(url: URL.init(string: urlStr)!)
        
        webView.loadRequest(urlRequest)
        webView.delegate = self
        activiryIndicator.startAnimating()
    }

    @IBAction func doneAction(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        
        activiryIndicator.stopAnimating()
        
    }
}
