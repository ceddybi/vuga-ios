//
//  InfoRegistartionViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import UIKit
import SVProgressHUD

class InfoRegistartionViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repeatPassword: UITextField!
    
    var params: RegisterParams!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }

    @IBAction func continueAction(_ sender: Any) {
        
        if valid() {
            
            if passwordValid() {
                
                params.firstname = firstName.text!
                params.lasttname = lastname.text!
                params.password = password.text!
                params.username = username.text!
                
                AuthManager.createAccount(username: params.username!,
                                          firstname: params.firstname!,
                                          lastname: params.lasttname!,
                                          phone: params.phone!,
                                          email: params.email!,
                                          password: params.password!,
                                          handler: {
                                            
                                            SVProgressHUD.showSuccess(withStatus: "Success")
                                            
                                            guard let destinationVC = self.getViewController(name: "ProfilePhotoViewController")
                                                else { return}
                                            
                                            self.present(destinationVC, animated: true)
                                            
                                            
                })
            }
            
        }
        
        
    }
    
    func passwordValid() -> Bool {
        
        if password.text! == repeatPassword.text {
            
            return true
        }
        
        self.showOkAlert(title: "Passwords do not equals", message: "")
        
        return false
    }
    
    func valid() -> Bool {
        
        if (firstName.text?.characters.count)! > 0,
            (lastname.text?.characters.count)! > 0,
        (username.text?.characters.count)! > 0,
        (password.text?.characters.count)! > 0,
        (repeatPassword.text?.characters.count)! > 0{
            
            return true
        }
        
        self.showOkAlert(title: "Fill all fields", message: "")
        
        return false
    }
}
