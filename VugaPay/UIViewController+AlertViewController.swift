//
//  UIViewController+AlertViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import Foundation
import UIKit

extension UIViewController {
    
    func showUpdateAlert(link: String, title: String, message: String) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "Ok", style: .default) { (_) in
            
            UIApplication.shared.openURL(URL.init(string: link)!)
        }
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true)
    }
    
    func showOkAlert(title: String, message: String) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction.init(title: "Ok", style: .cancel)
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true)
        
    }
    
    func showConfrimAlert(title: String, message: String, handler: @escaping (() -> Void)) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let noAction = UIAlertAction.init(title: "No", style: .cancel)
        
        let okAction = UIAlertAction.init(title: "Yes", style: .default) { (_) in
            
            handler()
        }
        
        alert.addAction(noAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true)
        
    }
    
    func showConfrimRejectAlert(title: String, message: String, user: User, handler: @escaping (() -> Void), noHandler: @escaping (() -> Void)) {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        let noAction = UIAlertAction.init(title: "No", style: .cancel){ (_) in
            
            noHandler()
        }
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 20, y: 8, width: 30, height: 30)
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
        imageView.setUserImage(strUrl: user.image_path)
        
        alert.view.addSubview(imageView)
        
        let okAction = UIAlertAction.init(title: "Yes", style: .default) { (_) in
            
            handler()
        }
        
        alert.addAction(noAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true)
        
    }
    
}
