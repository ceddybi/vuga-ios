//
//  PassportVerifyViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/25/17.
//
//

import UIKit
import ALCameraViewController
import SRMModalViewController

class PassportVerifyViewController: UIViewController {
    
    var image: UIImage?
    var creditCard = false

    @IBOutlet weak var passport: UIImageView!
    var handler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = image {

            self.passport.image = image
        }
    }
    
    @IBAction func tapImageView(_ sender: Any) {
        
        if let handler = handler {
            
            SRMModalViewController.sharedInstance().hide()
            
            handler()
        }
    }

    @IBAction func continueAction(_ sender: Any) {
        
        if let image = image {
            
            SRMModalViewController.sharedInstance().hide()
            
            if creditCard {
                
                SocketManager.shared.emitUploadCreditCardPhoto(image: image, handler: { 
                    
                })
                
            } else {
                
                SocketManager.shared.emitUploadPassportPhoto(image: image, handler: {
                    
                    
                    
                })
                
            }
            
            
        } else {
           
            
        }
        
    }
}
