//
//  LoginViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/9/17.
//
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func tapOnViewAction(_ sender: Any) {
        
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        AuthManager.login(login: userNameTextField.text!, password: passTextField.text!) {
         
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.mainFlow()
        }
        
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "RegistrationViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }
    
    @IBAction func forgotPassAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "ForgotPassViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }
    
}
