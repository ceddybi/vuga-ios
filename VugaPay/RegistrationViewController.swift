//
//  RegistrationViewController.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/9/17.
//
//

import UIKit

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var flagImageView: UIImageView!
    var code: UInt64?
    
    var phoneCode: String = "1"
    var region = "US"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let code = UserDefaults.standard.value(forKey: "code") as? UInt64 {
            
            self.code = code
        } else {
            
            code = AuthManager.randomNumber()
            
            UserDefaults.standard.set(code, forKey: "code")
        }

        PhoneFormatter.setTextFieldPlaceHolder(self.phoneTextField, region: "US")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.phoneTextField {
            
            PhoneFormatter.validateTextfield(textField: textField, region: region, range: range, withString: string)
        }
        
        return true
        
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        if PhoneFormatter.validatePhone(phone: self.phoneTextField.text!, region: region) {
        
            let phone = "+" + self.phoneCode + self.phoneTextField.text!
            
            self.showConfrimAlert(title: "Confirm?", message: "We are going to send sms code to \(phone)") {
                
                self.checkPhone()
                
            }
        } else {
            
            self.showOkAlert(title: "Wrong phone number", message: "")
        }
        
    }
    
    func checkPhone() {
        
        if let cc =  self.code {
         
            AuthManager.checkPhone(phone: phoneTextField.text!, code: cc) {
                
                self.checkEmail()
                
            }
        }
        
    }
    
    func checkEmail() {
        
        AuthManager.checkEmail(email: self.emailTextField.text!) { 
            
            self.sendSMS()
            
        }
        
    }
    
    func sendSMS() {
        
        let phone = "+" + self.phoneCode + self.phoneTextField.text!
        
        AuthManager.generateSMSCode(phone: phone, code: self.code!) { (code) in

            UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "time")
            
            guard let destinationVC = self.getViewController(name: "EnterCodeViewController"),
            let enterCodeVC = destinationVC as? EnterCodeViewController
                else { return }
            
            print("\(code)")
            
            enterCodeVC.code = "\(code)"
            var params = RegisterParams()
            params.phone = self.phoneTextField.text!
            params.email = self.emailTextField.text!
            enterCodeVC.params = params
            
            self.navigationController?.pushViewController(enterCodeVC, animated: true)
        }
        
    }
    

    @IBAction func termsAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "HelpViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "LoginViewController")
            else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
        
    }
    
    @IBAction func tapOnFlag(_ sender: Any) {
        
        guard let destinationVC = getViewController(name: "CountryUIViewController"),
        let countries = destinationVC as? CountryUIViewController
            else { return }
        
        countries.handlerCountry = { (countryDic, flag) in
            
            self.flagImageView.image = flag
            self.region = countryDic["country_code"] as! String!
            
            var code = "\(countryDic["phone_code"] as! Int)"
            
            if code.characters.last == "0" {
                
                code = String(code.characters.dropLast(1))
            }
            
            self.phoneCode = code
            PhoneFormatter.setTextFieldPlaceHolder(self.phoneTextField, region: countryDic["country_code"] as! String!)
            
        }
        
        self.present(destinationVC, animated: true)
        
    }
    
    
    @IBAction func tapOnView(_ sender: Any) {
        
        view.endEditing(true)
    }
}
