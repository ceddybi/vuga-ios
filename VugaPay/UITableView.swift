//
//  UITableView.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/24/17.
//
//

import Foundation
import UIKit

extension UITableView {
    
    func registerCell(name: String) {
        
        self.register(UINib.init(nibName: name, bundle: nil), forCellReuseIdentifier: name)
        self.rowHeight = 55
        
    }
    
    func userCell(at indexPath: IndexPath) -> UserTableViewCell {
        
        return self.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
    }
    
    func transactionCell(at indexPath: IndexPath) -> TransactionTableViewCell {
        
        return self.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
    }
}
