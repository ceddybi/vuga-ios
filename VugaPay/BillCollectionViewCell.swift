//
//  BillCollectionViewCell.swift
//  VugaPay
//
//  Created by Vladimir Hulchenko on 1/12/17.
//
//

import UIKit

class BillCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
}
