//
//  Cards+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Cards {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cards> {
        return NSFetchRequest<Cards>(entityName: "Cards");
    }

    @NSManaged public var card_number: String?
    @NSManaged public var cid: String?
    @NSManaged public var status: String?
    @NSManaged public var type: String?

}
