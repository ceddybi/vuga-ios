//
//  User+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var city: String?
    @NSManaged public var count_int: Int16
    @NSManaged public var email: String?
    @NSManaged public var firstname: String?
    @NSManaged public var image_path: String?
    @NSManaged public var isVerifyByAdmin: Int16
    @NSManaged public var lastname: String?
    @NSManaged public var password: String?
    @NSManaged public var phone: String?
    @NSManaged public var sex: String?
    @NSManaged public var state: String?
    @NSManaged public var status: String?
    @NSManaged public var userid: String?
    @NSManaged public var username: String?
    @NSManaged public var chat_int: Int16
    @NSManaged public var convocCo: Convos?
    @NSManaged public var convocCr: Convos?
    @NSManaged public var counter: Amount?
    @NSManaged public var request: Request?

}
