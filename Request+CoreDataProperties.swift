//
//  Request+CoreDataProperties.swift
//  
//
//  Created by Vladimir Hulchenko on 2/2/17.
//
//

import Foundation
import CoreData


extension Request {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Request> {
        return NSFetchRequest<Request>(entityName: "Request");
    }

    @NSManaged public var created_at: String?
    @NSManaged public var request_id: String?
    @NSManaged public var status: String?
    @NSManaged public var amount: Amount?
    @NSManaged public var from: User?

}
